<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLogTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('log', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedbigInteger('userId')->index();
            $table->foreign('userId')->references('id')->on('users');
            $table->unsignedBigInteger('postId')->index();
            $table->unsignedBigInteger('catId')->index();
            $table->unsignedBigInteger('mailId')->index();
            $table->foreign('postId')->references('id')->on('posts');
            $table->foreign('catId')->references('id')->on('categories');
            $table->foreign('mailId')->references('id')->on('mail')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('log');
    }
}
