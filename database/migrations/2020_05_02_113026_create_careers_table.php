<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCareersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('careers', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string("name")->nullable();
            $table->string("nameRu")->nullable();
            $table->string("nameEn")->nullable();
            $table->string("content")->nullable();
            $table->string("contentRu")->nullable();
            $table->string("contentEn")->nullable();
            $table->date("date")->nullable();
            $table->string("location")->nullable();
            $table->string("page")->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('careers');
    }
}
