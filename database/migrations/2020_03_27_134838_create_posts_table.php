<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePostsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('posts', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedbigInteger('userId')->index();
            $table->foreign('userId')->references('id')->on('users');
            $table->string('name')->nullable();
            $table->string('nameRu')->nullable();
            $table->string('nameEn')->nullable();
            $table->longText('content',255)->nullable();
            $table->longText('contentRu',255)->nullable();
            $table->longText('contentEn',255)->nullable();
            $table->string('image',255)->nullable();
            $table->string('type')->nullable();
            $table->string('status')->nullable();
            $table->string('page')->nullable();
            $table->string('link')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('posts');
    }
}
