<?php

return [
    "home" => "Главная",
    "aboutUs" => "О нас",
    "portfolio" => "Портфолио",
    "services" => "Услуги",
    "contactUs" => "Связь с нами",
    "partners" => "Партнеры",
    "fullName" => "Имя Фамилия",
    "callUs" => "Звоните",
    "findUs" => "Наш Адресс",
    "applyNow" => "Подать заявку сейчас",
    "jobDesc" => "Описание работы",
    "personalInfo" => "Личные данные",
    "headerSection" => "Верхняя секция",
    "mainSection" => "Средняя секция",
    "footerSection" => "Нижняя секция",
    "website" => "Вебсайт",
    "mailForSubs" => "Письмо всем подписчиком",
    "date"          => "Число",
    "location"      => "Место Нахождения",
    "qualifications" => "Квалификации",
    "createCareer" => "Создать кариеру",
    "content" => "Содержание",
    "dashboard" => "Панель Управления",
    "order" => "Заказ",
    "homeTitle" => "Добро Пожаловать",
    "LinkCVPortfolio" => "Ссылка портфолио или cv",
    "viewWebsite" => "Увидеть Вебсайт",
    "ReqQualifications" => "Требуемые квалификации",
    "categories" => "Категории",
    "sendEmail" => "Переписка",
    "subscribe" => "Подписка",
    "message" => "Сообщение",
    "readMore" => "Читать более",
    "description" => "Описание",
    "page" => "Страница",
    "delete" => "Удалить",
    "title" => "Заглавие",
    "image" => "Картинок",
    "send" => "Отправить",
    "name" => "Имя",
    'phone' => "Телефон:",
    "email" => "Эл. Почта:",
    "address" => "Адресс:",
    "save" => "Сохранить",
    "career" => "Кариера",
    "logoUpload" => "Выберите лого для сайта",
    "success_message" => "Отправлена Успешно!",
    "success_save" => "Сохранена успешно !",
    "error_message" => "При отправке заполните все поля!",
    "socialError" => "Введите что-нибуть",
    "websiteLogoValidation" => "Кортинок не соответсвует критериям , размер файла больше чем 200MB или размер больше чем 260x100 или не коректный файловый тип",
    "contactErr" => "Есть проблемы в секции контактов",
    "serviceError" => "Поля не соответсвуют критериям",
];