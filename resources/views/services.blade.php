@extends('layouts.main')

@section('content')
    <main class=" service">
        <div class="blueBackground"></div>
        <div class="myContainer">
            <h2 class="titleOfService">
                crm
            </h2>
            <div class="applyForService">
                <img src="{{asset("assets/images/whitebullets.png")}}" alt="">
                <form action="" id="serviceForm">
                    <span class="errorNoticeServices"></span>
                    <span class="serviceSuccess"></span>
                    <p class="title">{{ trans("settings.personalInfo") }}</p>
                    <div class="item">
                        <input type="text" name="serviceFullName" placeholder="{{ trans("settings.fullName") }}">
                        <input type="text" name="serviceWebsite" placeholder="{{ trans("settings.website") }}">
                        <input type="text" name="servicePhone" placeholder="{{ trans("settings.phone") }}">
                        <div class="dropdown dropdown2">
                            <div class="select">
                                <span class="text">{{ trans("settings.categories") }}</span>
                                <span class="icon">&#x3009;</span>
                            </div>
                            <input type="hidden" name="gender">
                            <ul class="dropdown-menu drop2">
                                @foreach($services as $service)
                                    <li class="serviceItem" data-id="{{ $service->id }}">{{ $service->name }}</li>
                                @endforeach
                            </ul>
                        </div>
                        <input type="email" name="serviceEmail" placeholder="{{ trans("settings.email") }}">
                        <div class="itemTextarea">
                            <label for="">{{ trans("settings.description") }}</label>
                            <textarea name="serviceText" placeholder="{{ trans("settings.message") }}" id="" cols="30" rows="10"></textarea>
                        </div>
                        <button type="button">{{ trans("settings.order") }}</button>
                    </div>
                </form>
            </div>
        </div>
    </main>
@stop

@section("pageTitle")
    {{ trans("settings.services") }}
@stop