@extends('layouts.main')

@section("content")
    <main class="carrersPage">
        <div class="myContainer">
            <h2>{{ trans("settings.career") }}</h2>
            <span class="successMessage @if(Session::has('success')) {{"hidable"}}@endif">
                 @if(Session::has('success'))
                    {!! Session::get('success') !!}
                @endif
            </span>
            <span class="careerErrorMessage">
                @if(Session::has('mailError'))
                    <p style="color: darkred">{!! Session::get('mailError')  !!}</p>
                @endif
            </span>
            <div class="careersItem">
                @foreach($careers as $career)
                    <div class="item">
                        <div class="title">
                            <p>{{ $career->name }}</p>
                        </div>
                        <div class="daTeAndAddress">
                            <div class="date">
                                <i class="far fa-calendar-alt"></i>
                                <span>{{\Carbon\Carbon::createFromFormat('Y-m-d', $career->date)->format('d/m/Y')}}</span>
                            </div>
                            <div class="address">
                                <i class="fas fa-map-marker-alt"></i>
                                <span>{{$career->location}}</span>
                            </div>
                        </div>
                        <div class="apply">
                            <button>{{ trans("settings.applyNow") }}</button>
                    </div>
                        <div class="applyedContent">
                            <div class="description">
                                <p class="title">{{ trans("settings.jobDesc") }}</p>
                                <div class="text">
                                    @if(Request::segment(1) == "hy")
                                        {!! $career->content  !!}
                                    @elseif(Request::segment(1) == "en")
                                        {!! $career->contentEn !!}
                                    @else
                                        {!! $career->contentRu !!}
                                    @endif
                                </div>
                            </div>
                            <div class="qualifications">
                                <p class="title">{{ trans("settings.ReqQualifications") }}</p>
                                <ul>
                                    @foreach($career->category as $category)
                                        <li>
			 							<span>
			 								<i class="fas fa-check"></i>
			 							</span> {{ $category->name }}
                                        </li>
                                    @endforeach
                                </ul>
                            </div>
                            <div class="applyForJobForm">

                                <form action="" method="POST" enctype="multipart/form-data">
                                    @csrf

{{--                                    @if(Session::has('exception'))--}}
{{--                                        <p style="color: forestgreen;">{!! Session::get('exception') !!}</p>--}}
{{--                                    @endif--}}


                                    <input type="hidden" name="position" value="{{ $career->name }}">
                                    <div class="inputs">
                                        @error('careerName')
                                        <h4>Error Name</h4>
                                        @endError
                                        <div class="itemInp">
                                            <label for="">{{ trans("settings.name") }}</label>
                                            <input type="text" name="careerName" placeholder="{{ trans("settings.name") }}...">
                                        </div>
                                        <div class="itemInp">
                                            @error('careerEmail')
                                            <h4>Error Email</h4>
                                            @endError
                                            <label for="">{{ trans("settings.email") }}</label>
                                            <input type="text" name="careerEmail" placeholder="{{ trans("settings.email") }}...">
                                        </div>
                                        <div class="itemInp cvFile">
                                            @error('cvFile')
                                            <h4>File not exists or not valid</h4>
                                            @endError
                                            <label class="lblForCv">
                                                <div class="cvSpan">Choose the file of CV or Portfolio</div>
                                                <span class="noChoosen">No File Choosen</span>
                                                <input type="file" name="cvFile" accept="application/msword, application/vnd.ms-excel, application/pdf," placeholder="">
                                            </label>
                                        </div>
                                        <div class="itemInp">
                                            <label for="">{{ trans("settings.LinkCVPortfolio") }}</label>
                                            <input type="text" name="cvLink" placeholder="Enter Your Link of CV or Portfolio...">
                                        </div>
                                        <div class="applyButton">
                                            <button type="submit">{{ trans("settings.send") }}</button>
                                        </div>
                                    </div>
                                    <div class="messages">
                                        <label>{{ trans("settings.message") }}</label>
                                        <textarea name="careerMessage" cols="30" rows="10" placeholder="..."></textarea>

                                    </div>
                                </form>
                                <span class="closeOPened">
			 						<i class="fas fa-chevron-up"></i>
			 					</span>
                            </div>
                        </div>
                    </div>
                @endforeach

{{--                <div class="item">--}}
{{--                    <div class="title">--}}
{{--                        <p>Front-End Developer</p>--}}
{{--                    </div>--}}
{{--                    <div class="daTeAndAddress">--}}
{{--                        <div class="date">--}}
{{--                            <i class="far fa-calendar-alt"></i>--}}
{{--                            <span>13/04/2020</span>--}}
{{--                        </div>--}}
{{--                        <div class="address">--}}
{{--                            <i class="fas fa-map-marker-alt"></i>--}}
{{--                            <span>Yerevan</span>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                    <div class="apply">--}}
{{--                        <button>Apply Now</button>--}}
{{--                    </div>--}}
{{--                    <div class="applyedContent">--}}
{{--                        <div class="description">--}}
{{--                            <p class="title">Job Description:</p>--}}
{{--                            <p class="text">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum. Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more.to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more.to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more.</p>--}}
{{--                        </div>--}}
{{--                        <div class="qualifications">--}}
{{--                            <p class="title">Required Qualifications</p>--}}
{{--                            <ul>--}}
{{--                                <li>--}}
{{--			 							<span>--}}
{{--			 								<i class="fas fa-check"></i>--}}
{{--			 							</span> Lorem Ipsum--}}
{{--                                </li>--}}
{{--                                <li>--}}
{{--			 							<span>--}}
{{--			 								<i class="fas fa-check"></i>--}}
{{--			 							</span> Lorem Ipsum--}}
{{--                                </li>--}}
{{--                                <li>--}}
{{--			 							<span>--}}
{{--			 								<i class="fas fa-check"></i>--}}
{{--			 							</span> Lorem Ipsum--}}
{{--                                </li>--}}
{{--                                <li>--}}
{{--			 							<span>--}}
{{--			 								<i class="fas fa-check"></i>--}}
{{--			 							</span> Lorem Ipsum--}}
{{--                                </li>--}}
{{--                                <li>--}}
{{--			 							<span>--}}
{{--			 								<i class="fas fa-check"></i>--}}
{{--			 							</span> Lorem Ipsum--}}
{{--                                </li>--}}
{{--                                <li>--}}
{{--			 							<span>--}}
{{--			 								<i class="fas fa-check"></i>--}}
{{--			 							</span> Lorem Ipsum--}}
{{--                                </li>--}}
{{--                            </ul>--}}
{{--                        </div>--}}
{{--                        <div class="applyForJobForm">--}}
{{--                            <form action="">--}}
{{--                                <div class="inputs">--}}
{{--                                    <div class="itemInp">--}}
{{--                                        <label for="">Name</label>--}}
{{--                                        <input type="text" placeholder="Enter Your Name...">--}}
{{--                                    </div>--}}
{{--                                    <div class="itemInp">--}}
{{--                                        <label for="">E-mail</label>--}}
{{--                                        <input type="text" placeholder="Enter Your E-mail Address...">--}}
{{--                                    </div>--}}
{{--                                    <div class="itemInp cvFile">--}}
{{--                                        <label for="">Choose the file of CV or Portfolio</label>--}}
{{--                                        <span>No File Choosen</span>--}}
{{--                                        <input type="file" placeholder="">--}}
{{--                                    </div>--}}
{{--                                    <div class="itemInp ">--}}
{{--                                        <label for="">Link of CV or Portfolio</label>--}}
{{--                                        <input type="text" placeholder="Enter Your Link of CV or Portfolio...">--}}
{{--                                    </div>--}}
{{--                                    <div class="applyButton">--}}
{{--                                        <button>Send</button>--}}
{{--                                    </div>--}}
{{--                                </div>--}}
{{--                                <div class="messages">--}}
{{--                                    <label for="">Message</label>--}}
{{--                                    <textarea name="" id="" cols="30" rows="10" placeholder="..."></textarea>--}}
{{--                                </div>--}}
{{--                            </form>--}}
{{--                            <span class="closeOPened">--}}
{{--			 						<i class="fas fa-chevron-up"></i>--}}
{{--			 					</span>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--                <div class="item">--}}
{{--                    <div class="title">--}}
{{--                        <p>Front-End Developer</p>--}}
{{--                    </div>--}}
{{--                    <div class="daTeAndAddress">--}}
{{--                        <div class="date">--}}
{{--                            <i class="far fa-calendar-alt"></i>--}}
{{--                            <span>13/04/2020</span>--}}
{{--                        </div>--}}
{{--                        <div class="address">--}}
{{--                            <i class="fas fa-map-marker-alt"></i>--}}
{{--                            <span>Yerevan</span>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                    <div class="apply">--}}
{{--                        <button>Apply Now</button>--}}
{{--                    </div>--}}
{{--                    <div class="applyedContent">--}}
{{--                        <div class="description">--}}
{{--                            <p class="title">Job Description:</p>--}}
{{--                            <p class="text">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum. Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more.to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more.to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more.</p>--}}
{{--                        </div>--}}
{{--                        <div class="qualifications">--}}
{{--                            <p class="title">Required Qualifications</p>--}}
{{--                            <ul>--}}
{{--                                <li>--}}
{{--			 							<span>--}}
{{--			 								<i class="fas fa-check"></i>--}}
{{--			 							</span> Lorem Ipsum--}}
{{--                                </li>--}}
{{--                                <li>--}}
{{--			 							<span>--}}
{{--			 								<i class="fas fa-check"></i>--}}
{{--			 							</span> Lorem Ipsum--}}
{{--                                </li>--}}
{{--                                <li>--}}
{{--			 							<span>--}}
{{--			 								<i class="fas fa-check"></i>--}}
{{--			 							</span> Lorem Ipsum--}}
{{--                                </li>--}}
{{--                                <li>--}}
{{--			 							<span>--}}
{{--			 								<i class="fas fa-check"></i>--}}
{{--			 							</span> Lorem Ipsum--}}
{{--                                </li>--}}
{{--                                <li>--}}
{{--			 							<span>--}}
{{--			 								<i class="fas fa-check"></i>--}}
{{--			 							</span> Lorem Ipsum--}}
{{--                                </li>--}}
{{--                                <li>--}}
{{--			 							<span>--}}
{{--			 								<i class="fas fa-check"></i>--}}
{{--			 							</span> Lorem Ipsum--}}
{{--                                </li>--}}
{{--                            </ul>--}}
{{--                        </div>--}}
{{--                        <div class="applyForJobForm">--}}
{{--                            <form action="">--}}
{{--                                <div class="inputs">--}}
{{--                                    <div class="itemInp">--}}
{{--                                        <label for="">Name</label>--}}
{{--                                        <input type="text" placeholder="Enter Your Name...">--}}
{{--                                    </div>--}}
{{--                                    <div class="itemInp">--}}
{{--                                        <label for="">E-mail</label>--}}
{{--                                        <input type="text" placeholder="Enter Your E-mail Address...">--}}
{{--                                    </div>--}}
{{--                                    <div class="itemInp cvFile">--}}
{{--                                        <label for="">Choose the file of CV or Portfolio</label>--}}
{{--                                        <span>No File Choosen</span>--}}
{{--                                        <input type="file" placeholder="">--}}
{{--                                    </div>--}}
{{--                                    <div class="itemInp ">--}}
{{--                                        <label for="">Link of CV or Portfolio</label>--}}
{{--                                        <input type="text" placeholder="Enter Your Link of CV or Portfolio...">--}}
{{--                                    </div>--}}
{{--                                    <div class="applyButton">--}}
{{--                                        <button>Send</button>--}}
{{--                                    </div>--}}
{{--                                </div>--}}
{{--                                <div class="messages">--}}
{{--                                    <label for="">Message</label>--}}
{{--                                    <textarea name="" id="" cols="30" rows="10" placeholder="..."></textarea>--}}
{{--                                </div>--}}
{{--                            </form>--}}
{{--                            <span class="closeOPened">--}}
{{--			 						<i class="fas fa-chevron-up"></i>--}}
{{--			 					</span>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--                <div class="item">--}}
{{--                    <div class="title">--}}
{{--                        <p>Front-End Developer</p>--}}
{{--                    </div>--}}
{{--                    <div class="daTeAndAddress">--}}
{{--                        <div class="date">--}}
{{--                            <i class="far fa-calendar-alt"></i>--}}
{{--                            <span>13/04/2020</span>--}}
{{--                        </div>--}}
{{--                        <div class="address">--}}
{{--                            <i class="fas fa-map-marker-alt"></i>--}}
{{--                            <span>Yerevan</span>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                    <div class="apply">--}}
{{--                        <button>Apply Now</button>--}}
{{--                    </div>--}}
{{--                    <div class="applyedContent">--}}
{{--                        <div class="description">--}}
{{--                            <p class="title">Job Description:</p>--}}
{{--                            <p class="text">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum. Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more.to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more.to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more.</p>--}}
{{--                        </div>--}}
{{--                        <div class="qualifications">--}}
{{--                            <p class="title">Required Qualifications</p>--}}
{{--                            <ul>--}}
{{--                                <li>--}}
{{--			 							<span>--}}
{{--			 								<i class="fas fa-check"></i>--}}
{{--			 							</span> Lorem Ipsum--}}
{{--                                </li>--}}
{{--                                <li>--}}
{{--			 							<span>--}}
{{--			 								<i class="fas fa-check"></i>--}}
{{--			 							</span> Lorem Ipsum--}}
{{--                                </li>--}}
{{--                                <li>--}}
{{--			 							<span>--}}
{{--			 								<i class="fas fa-check"></i>--}}
{{--			 							</span> Lorem Ipsum--}}
{{--                                </li>--}}
{{--                                <li>--}}
{{--			 							<span>--}}
{{--			 								<i class="fas fa-check"></i>--}}
{{--			 							</span> Lorem Ipsum--}}
{{--                                </li>--}}
{{--                                <li>--}}
{{--			 							<span>--}}
{{--			 								<i class="fas fa-check"></i>--}}
{{--			 							</span> Lorem Ipsum--}}
{{--                                </li>--}}
{{--                                <li>--}}
{{--			 							<span>--}}
{{--			 								<i class="fas fa-check"></i>--}}
{{--			 							</span> Lorem Ipsum--}}
{{--                                </li>--}}
{{--                            </ul>--}}
{{--                        </div>--}}
{{--                        <div class="applyForJobForm">--}}
{{--                            <form action="">--}}
{{--                                <div class="inputs">--}}
{{--                                    <div class="itemInp">--}}
{{--                                        <label for="">Name</label>--}}
{{--                                        <input type="text" placeholder="Enter Your Name...">--}}
{{--                                    </div>--}}
{{--                                    <div class="itemInp">--}}
{{--                                        <label for="">E-mail</label>--}}
{{--                                        <input type="text" placeholder="Enter Your E-mail Address...">--}}
{{--                                    </div>--}}
{{--                                    <div class="itemInp cvFile">--}}
{{--                                        <label for="">Choose the file of CV or Portfolio</label>--}}
{{--                                        <span>No File Choosen</span>--}}
{{--                                        <input type="file" placeholder="">--}}
{{--                                    </div>--}}
{{--                                    <div class="itemInp ">--}}
{{--                                        <label for="">Link of CV or Portfolio</label>--}}
{{--                                        <input type="text" placeholder="Enter Your Link of CV or Portfolio...">--}}
{{--                                    </div>--}}
{{--                                    <div class="applyButton">--}}
{{--                                        <button>Send</button>--}}
{{--                                    </div>--}}
{{--                                </div>--}}
{{--                                <div class="messages">--}}
{{--                                    <label for="">Message</label>--}}
{{--                                    <textarea name="" id="" cols="30" rows="10" placeholder="..."></textarea>--}}
{{--                                </div>--}}
{{--                            </form>--}}
{{--                            <span class="closeOPened">--}}
{{--			 						<i class="fas fa-chevron-up"></i>--}}
{{--			 					</span>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--                <div class="item">--}}
{{--                    <div class="title">--}}
{{--                        <p>Front-End Developer</p>--}}
{{--                    </div>--}}
{{--                    <div class="daTeAndAddress">--}}
{{--                        <div class="date">--}}
{{--                            <i class="far fa-calendar-alt"></i>--}}
{{--                            <span>13/04/2020</span>--}}
{{--                        </div>--}}
{{--                        <div class="address">--}}
{{--                            <i class="fas fa-map-marker-alt"></i>--}}
{{--                            <span>Yerevan</span>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                    <div class="apply">--}}
{{--                        <button>Apply Now</button>--}}
{{--                    </div>--}}
{{--                    <div class="applyedContent">--}}
{{--                        <div class="description">--}}
{{--                            <p class="title">Job Description:</p>--}}
{{--                            <p class="text">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum. Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more.to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more.to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more.</p>--}}
{{--                        </div>--}}
{{--                        <div class="qualifications">--}}
{{--                            <p class="title">Required Qualifications</p>--}}
{{--                            <ul>--}}
{{--                                <li>--}}
{{--			 							<span>--}}
{{--			 								<i class="fas fa-check"></i>--}}
{{--			 							</span> Lorem Ipsum--}}
{{--                                </li>--}}
{{--                                <li>--}}
{{--			 							<span>--}}
{{--			 								<i class="fas fa-check"></i>--}}
{{--			 							</span> Lorem Ipsum--}}
{{--                                </li>--}}
{{--                                <li>--}}
{{--			 							<span>--}}
{{--			 								<i class="fas fa-check"></i>--}}
{{--			 							</span> Lorem Ipsum--}}
{{--                                </li>--}}
{{--                                <li>--}}
{{--			 							<span>--}}
{{--			 								<i class="fas fa-check"></i>--}}
{{--			 							</span> Lorem Ipsum--}}
{{--                                </li>--}}
{{--                                <li>--}}
{{--			 							<span>--}}
{{--			 								<i class="fas fa-check"></i>--}}
{{--			 							</span> Lorem Ipsum--}}
{{--                                </li>--}}
{{--                                <li>--}}
{{--			 							<span>--}}
{{--			 								<i class="fas fa-check"></i>--}}
{{--			 							</span> Lorem Ipsum--}}
{{--                                </li>--}}
{{--                            </ul>--}}
{{--                        </div>--}}
{{--                        <div class="applyForJobForm">--}}
{{--                            <form action="">--}}
{{--                                <div class="inputs">--}}
{{--                                    <div class="itemInp">--}}
{{--                                        <label for="">Name</label>--}}
{{--                                        <input type="text" placeholder="Enter Your Name...">--}}
{{--                                    </div>--}}
{{--                                    <div class="itemInp">--}}
{{--                                        <label for="">E-mail</label>--}}
{{--                                        <input type="text" placeholder="Enter Your E-mail Address...">--}}
{{--                                    </div>--}}
{{--                                    <div class="itemInp cvFile">--}}
{{--                                        <label for="">Choose the file of CV or Portfolio</label>--}}
{{--                                        <span>No File Choosen</span>--}}
{{--                                        <input type="file" placeholder="">--}}
{{--                                    </div>--}}
{{--                                    <div class="itemInp ">--}}
{{--                                        <label for="">Link of CV or Portfolio</label>--}}
{{--                                        <input type="text" placeholder="Enter Your Link of CV or Portfolio...">--}}
{{--                                    </div>--}}
{{--                                    <div class="applyButton">--}}
{{--                                        <button>Send</button>--}}
{{--                                    </div>--}}
{{--                                </div>--}}
{{--                                <div class="messages">--}}
{{--                                    <label for="">Message</label>--}}
{{--                                    <textarea name="" id="" cols="30" rows="10" placeholder="..."></textarea>--}}
{{--                                </div>--}}
{{--                            </form>--}}
{{--                            <span class="closeOPened">--}}
{{--			 						<i class="fas fa-chevron-up"></i>--}}
{{--			 					</span>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--                <div class="item">--}}
{{--                    <div class="title">--}}
{{--                        <p>Front-End Developer</p>--}}
{{--                    </div>--}}
{{--                    <div class="daTeAndAddress">--}}
{{--                        <div class="date">--}}
{{--                            <i class="far fa-calendar-alt"></i>--}}
{{--                            <span>13/04/2020</span>--}}
{{--                        </div>--}}
{{--                        <div class="address">--}}
{{--                            <i class="fas fa-map-marker-alt"></i>--}}
{{--                            <span>Yerevan</span>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                    <div class="apply">--}}
{{--                        <button>Apply Now</button>--}}
{{--                    </div>--}}
{{--                    <div class="applyedContent">--}}
{{--                        <div class="description">--}}
{{--                            <p class="title">Job Description:</p>--}}
{{--                            <p class="text">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum. Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more.to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more.to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more.</p>--}}
{{--                        </div>--}}
{{--                        <div class="qualifications">--}}
{{--                            <p class="title">Required Qualifications</p>--}}
{{--                            <ul>--}}
{{--                                <li>--}}
{{--			 							<span>--}}
{{--			 								<i class="fas fa-check"></i>--}}
{{--			 							</span> Lorem Ipsum--}}
{{--                                </li>--}}
{{--                                <li>--}}
{{--			 							<span>--}}
{{--			 								<i class="fas fa-check"></i>--}}
{{--			 							</span> Lorem Ipsum--}}
{{--                                </li>--}}
{{--                                <li>--}}
{{--			 							<span>--}}
{{--			 								<i class="fas fa-check"></i>--}}
{{--			 							</span> Lorem Ipsum--}}
{{--                                </li>--}}
{{--                                <li>--}}
{{--			 							<span>--}}
{{--			 								<i class="fas fa-check"></i>--}}
{{--			 							</span> Lorem Ipsum--}}
{{--                                </li>--}}
{{--                                <li>--}}
{{--			 							<span>--}}
{{--			 								<i class="fas fa-check"></i>--}}
{{--			 							</span> Lorem Ipsum--}}
{{--                                </li>--}}
{{--                                <li>--}}
{{--			 							<span>--}}
{{--			 								<i class="fas fa-check"></i>--}}
{{--			 							</span> Lorem Ipsum--}}
{{--                                </li>--}}
{{--                            </ul>--}}
{{--                        </div>--}}
{{--                        <div class="applyForJobForm">--}}
{{--                            <form action="">--}}
{{--                                <div class="inputs">--}}
{{--                                    <div class="itemInp">--}}
{{--                                        <label for="">Name</label>--}}
{{--                                        <input type="text" placeholder="Enter Your Name...">--}}
{{--                                    </div>--}}
{{--                                    <div class="itemInp">--}}
{{--                                        <label for="">E-mail</label>--}}
{{--                                        <input type="text" placeholder="Enter Your E-mail Address...">--}}
{{--                                    </div>--}}
{{--                                    <div class="itemInp cvFile">--}}
{{--                                        <label for="">Choose the file of CV or Portfolio</label>--}}
{{--                                        <span>No File Choosen</span>--}}
{{--                                        <input type="file" placeholder="">--}}
{{--                                    </div>--}}
{{--                                    <div class="itemInp ">--}}
{{--                                        <label for="">Link of CV or Portfolio</label>--}}
{{--                                        <input type="text" placeholder="Enter Your Link of CV or Portfolio...">--}}
{{--                                    </div>--}}
{{--                                    <div class="applyButton">--}}
{{--                                        <button>Send</button>--}}
{{--                                    </div>--}}
{{--                                </div>--}}
{{--                                <div class="messages">--}}
{{--                                    <label for="">Message</label>--}}
{{--                                    <textarea name="" id="" cols="30" rows="10" placeholder="..."></textarea>--}}
{{--                                </div>--}}
{{--                            </form>--}}
{{--                            <span class="closeOPened">--}}
{{--			 						<i class="fas fa-chevron-up"></i>--}}
{{--			 					</span>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                </div>--}}

            </div>
        </div>
    </main>
@stop

@section("pageTitle")
    {{ trans("settings.career") }}
@stop