@extends('layouts.main')

@section('content')

    <main class="carrersPage portfolio">
        <div class="myContainer">
            <h2>{{ trans("settings.portfolio") }}</h2>
            <div class="filters">
                <button class="resetActive active">All</button>
{{--                <button>Web Design</button>--}}
{{--                <button>Web Design</button>--}}
{{--                <button>Web Design</button>--}}
{{--                <button>Web Design</button>--}}
{{--                <button>Web Design</button>--}}
{{--                <button>Web Design</button>--}}
                @foreach($categories as $category)
                    <button class="" data-id="{{ $category->id }}">{{ $category->name }}</button>
                @endforeach
            </div>
            <section class="elements">
                @include('includes.portfolioPagination')
            </section>
        </div>
    </main>
@endsection
@section("pageTitle")
    {{ trans("settings.portfolio") }}
@stop