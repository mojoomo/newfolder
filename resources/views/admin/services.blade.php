@extends('admin.dashboard')

@section('admContent')
    <div class="col-md-8 servicesPageAdmin">
        <h1>Services page</h1>
        <div class="content-box-large">
            <div class="panel-heading">
                <div class="panel-title">
                    Show Orders
                </div>
            </div>
            <div class="panel-body">
                @foreach($orders as $order)
                    <section class="applicantFullInfo">
                        <span class="removeApplicant" data-id="{{ $order->id  }}">
                            <svg viewBox="0 0 1792 1792" fill="currentColor" xmlns="http://www.w3.org/2000/svg"><path d="M1490 1322q0 40-28 68l-136 136q-28 28-68 28t-68-28l-294-294-294 294q-28 28-68 28t-68-28l-136-136q-28-28-28-68t28-68l294-294-294-294q-28-28-28-68t28-68l136-136q28-28 68-28t68 28l294 294 294-294q28-28 68-28t68 28l136 136q28 28 28 68t-28 68l-294 294 294 294q28 28 28 68z"></path></svg>
                        </span>

                        <div class="applicants">
                            <div class="form-group applicantInfo">
                                <span class="title"><b>Order Date</b></span><br>
                                <span class="time">{{ $order->created_at }}</span>
                            </div>
                            <div class="form-group applicantInfo">
                                <span class="applicentIp"><b>Applicent Ip</b></span><br>
                                <span class="ip">{{ $order->ip }}</span>
                            </div>
                            <div class="form-group applicantInfo">
                                <span class="applicentName"><b>Applicent name</b></span><br>
                                <span class="fullName">{{ $order->fullname }}</span>
                            </div>
                            <div class="form-group applicantInfo">
                                <span class="applicentPhone"><b>Applicent Phone</b></span><br>
                                <span class="phone">{{ $order->phone }}</span>
                            </div>
                            <div class="form-group applicantInfo">
                                <span class="applicentWebsite"><b>Applicent Website</b></span><br>
                                <span class="website">{{ $order->website }}</span>
                            </div>
                            <div class="form-group applicantInfo">
                                <span class="applicentCategory"><b>Applicent Order</b></span><br>
                                <span class="category">{{ $order->cat }}</span>
                            </div>
                            <div class="form-group applicantInfo">
                                <span class="applicentEmail"><b>Applicent email</b></span><br>
                                <span class="email">{{ $order->email }}</span>
                            </div>
                            <div class="form-group applicantInfo">
                                <span class="applicentMesssage"><b>Applicent Message</b></span><br>
                                <span class="message">
                                    {{ $order->message }}
                                </span>
                            </div>
                        </div>
                    </section>
                @endforeach
{{--               <section class="applicantFullInfo">--}}
{{--                   <span class="removeApplicant" data-id="">--}}
{{--                       <svg viewBox="0 0 1792 1792" fill="currentColor" xmlns="http://www.w3.org/2000/svg"><path d="M1490 1322q0 40-28 68l-136 136q-28 28-68 28t-68-28l-294-294-294 294q-28 28-68 28t-68-28l-136-136q-28-28-28-68t28-68l294-294-294-294q-28-28-28-68t28-68l136-136q28-28 68-28t68 28l294 294 294-294q28-28 68-28t68 28l136 136q28 28 28 68t-28 68l-294 294 294 294q28 28 28 68z"></path></svg>--}}
{{--                   </span>--}}
{{--                   <div class="orderCreate">--}}
{{--                       <span class="title"><b>Order Date</b></span>--}}
{{--                       <span class="time"></span>--}}
{{--                   </div>--}}
{{--                   <div class="applicants">--}}
{{--                       <div class="form-group applicantInfo">--}}
{{--                           <span class="applicentIp"><b>Applicent Ip</b></span><br>--}}
{{--                           <span class="ip">434654</span>--}}
{{--                       </div>--}}
{{--                       <div class="form-group applicantInfo">--}}
{{--                           <span class="applicentName"><b>Applicent name</b></span><br>--}}
{{--                           <span class="fullName">asdfasdgaSDASDF</span>--}}
{{--                       </div>--}}
{{--                       <div class="form-group applicantInfo">--}}
{{--                           <span class="applicentPhone"><b>Applicent Phone</b></span><br>--}}
{{--                           <span class="phone">413465437</span>--}}
{{--                       </div>--}}
{{--                       <div class="form-group applicantInfo">--}}
{{--                           <span class="applicentWebsite"><b>Applicent Website</b></span><br>--}}
{{--                           <span class="website">aasdfasdfg.com</span>--}}
{{--                       </div>--}}
{{--                       <div class="form-group applicantInfo">--}}
{{--                           <span class="applicentCategory"><b>Applicent Order</b></span><br>--}}
{{--                           <span class="category">Web Development</span>--}}
{{--                       </div>--}}
{{--                       <div class="form-group applicantInfo">--}}
{{--                           <span class="applicentEmail"><b>Applicent email</b></span><br>--}}
{{--                           <span class="email">asfadsg@asdf.com</span>--}}
{{--                       </div>--}}
{{--                       <div class="form-group applicantInfo">--}}
{{--                           <span class="applicentMesssage"><b>Applicent Message</b></span><br>--}}
{{--                           <span class="message">--}}
{{--                            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquam aperiam aut autem cum, eius expedita ipsa iure iusto minima modi mollitia nobis nulla, quidem quis quos rem reprehenderit tempore, unde vel voluptas! Accusantium adipisci alias assumenda aut cum deleniti deserunt dicta dignissimos earum enim error eum expedita facilis hic id inventore laboriosam magni minus, molestias necessitatibus nostrum odio omnis pariatur placeat possimus qui quis quisquam quo reprehenderit, sapiente sequi soluta tempore ullam vitae voluptate voluptatem voluptatum. Adipisci commodi excepturi ipsa laboriosam, minima nisi numquam sequi tenetur velit veritatis. Amet deleniti, dolore illo minima possimus quas rem saepe suscipit totam vero.--}}
{{--                        </span>--}}
{{--                       </div>--}}
{{--                   </div>--}}
{{--               </section>--}}
            </div>
        </div>
    </div>
@stop

@section("admTitle")
    {{ trans("settings.services") }}
@stop