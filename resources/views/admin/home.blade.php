@extends('admin.dashboard')

@section('admContent')
    <div class="col-md-8">
        <h1>Home page</h1>
        <div class="content-box-large homeHead">
            <div class="panel-heading">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-3 pull-right">
                            <h4>{{trans("settings.logoUpload")}}</h4>
                            <form id="logoForm" action="uploadLogo" method="POST" enctype="multipart/form-data">
                                @csrf
                                <div class="panel-title">
                                    <div class="form-group">
                                        <label >
                                            <img src="{{asset("assets/images/file-upload.svg")}}" class="imageUploadIcon" alt="">
                                            <input type="file" id="pageLogo" name="imageLogo" class="form-control">
                                        </label>
                                    </div>
                                </div>
                            </form>
                            <div class="panel-title logoPreview">
                                @if(Session::has("imagePath"))
                                    <img src="{{ Session::get('imagePath') }}" alt="">
                                @elseif(isset($logoPath[0]))
                                    <img src="{{ json_decode($logoPath[0])->image }}" alt="">
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
                <h3 align="center">{{ trans("settings.headerSection") }}</h3>
                <div class="panel-title">
                    <div class="form-group">
                        <span align="right"><span>{{trans("settings.title")}}</span>
                            @if(Request::segment(1) == "hy")
                                <input type="text" class="homeHeaderPostTitle form-control" data-lang="hy">
                            @elseif(Request::segment(1) == "en")
                                <input type="text" class="homeHeaderPostTitle form-control" data-lang="en">
                            @else
                                <input type="text" class="homeHeaderPostTitle form-control" data-lang="ru">
                            @endif
                        </span>
                    </div>
                </div>
                <div class="panel-title">
                    <span>{{ trans("settings.image") }}</span>
                    <div class="form-group float-md-right">
                        &nbsp;
                        <label>
                            <img src="{{asset("assets/images/file-upload.svg")}}" class="imageUploadIcon" alt="">
                            <input type="file" id="headPostImage" name="headPostImage" class="form-control">
                        </label>
                    </div>
                </div>
                <div class="panel-title">
                    <div class="headPostImage">
                        <img src="" alt="">
                    </div>
                </div>
                <div class="panel-title">
                    <div class="deleteButtonblock">
                        <span class="deleteHeader deleteContainer" data-id="">
                            <a href="javascript:void(0);">{{ trans('settings.delete') }}</a>
                        </span>
                    </div>
                </div>
                {{--                <div class="panel-options">--}}
                {{--                    <a href="#" data-rel="collapse"><i class="glyphicon glyphicon-refresh"></i></a>--}}
                {{--                    <a href="#" data-rel="reload"><i class="glyphicon glyphicon-cog"></i></a>--}}
                {{--                </div>--}}
            </div>
            <form action="javascript: void(0);" class="formOne" method="POST">
                <div class="panel-body">
                    {{--                    <textarea id="tinymce_full"></textarea>--}}
                    @if(Request::segment(1) == "hy")
                        <textarea id="homeEditor1_hy"></textarea>
                    @elseif(Request::segment(1) == "en")
                        <textarea id="homeEditor1_en"></textarea>
                    @else
                        <textarea id="homeEditor1_ru"></textarea>
                    @endif
                </div>
            </form>

            <div class="panel-heading">
                <div class="panel-title">

                </div>
            </div>
            {{--            <div class="panel-body">--}}
            {{--                <ul class="linksList homeHeaderList">--}}
            {{--                    <li class="item fb">--}}
            {{--                        --}}{{--                       <img src="data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHZpZXdCb3g9IjAgMCAzMjAg%0D%0ANTEyIj48cGF0aCBkPSJNMjc5LjE0IDI4OGwxNC4yMi05Mi42NmgtODguOTF2LTYwLjEzYzAtMjUu%0D%0AMzUgMTIuNDItNTAuMDYgNTIuMjQtNTAuMDZoNDAuNDJWNi4yNlMyNjAuNDMgMCAyMjUuMzYgMGMt%0D%0ANzMuMjIgMC0xMjEuMDggNDQuMzgtMTIxLjA4IDEyNC43MnY3MC42MkgyMi44OVYyODhoODEuMzl2%0D%0AMjI0aDEwMC4xN1YyODh6Ii8+PC9zdmc+" alt="">--}}
            {{--                        <span class="image">--}}
            {{--                           <svg viewBox="0 0 1792 1792" xmlns="http://www.w3.org/2000/svg"><path--}}
            {{--                                       d="M1343 12v264h-157q-86 0-116 36t-30 108v189h293l-39 296h-254v759h-306v-759h-255v-296h255v-218q0-186 104-288.5t277-102.5q147 0 228 12z"--}}
            {{--                                       fill="#dedede"/></svg>--}}
            {{--                       </span>--}}
            {{--                        <div class="form-group">--}}
            {{--                            <span class="link fb" data-id=""><input type="url" class="form-control"--}}
            {{--                                                                    placeholder="Url"></span>--}}
            {{--                        </div>--}}
            {{--                    </li>--}}
            {{--                    <li class="item inst">--}}
            {{--                       <span class="image">--}}
            {{--                           <svg viewBox="0 0 1792 1792" xmlns="http://www.w3.org/2000/svg"><path--}}
            {{--                                       d="M1152 896q0-106-75-181t-181-75-181 75-75 181 75 181 181 75 181-75 75-181zm138 0q0 164-115 279t-279 115-279-115-115-279 115-279 279-115 279 115 115 279zm108-410q0 38-27 65t-65 27-65-27-27-65 27-65 65-27 65 27 27 65zm-502-220q-7 0-76.5-.5t-105.5 0-96.5 3-103 10-71.5 18.5q-50 20-88 58t-58 88q-11 29-18.5 71.5t-10 103-3 96.5 0 105.5.5 76.5-.5 76.5 0 105.5 3 96.5 10 103 18.5 71.5q20 50 58 88t88 58q29 11 71.5 18.5t103 10 96.5 3 105.5 0 76.5-.5 76.5.5 105.5 0 96.5-3 103-10 71.5-18.5q50-20 88-58t58-88q11-29 18.5-71.5t10-103 3-96.5 0-105.5-.5-76.5.5-76.5 0-105.5-3-96.5-10-103-18.5-71.5q-20-50-58-88t-88-58q-29-11-71.5-18.5t-103-10-96.5-3-105.5 0-76.5.5zm768 630q0 229-5 317-10 208-124 322t-322 124q-88 5-317 5t-317-5q-208-10-322-124t-124-322q-5-88-5-317t5-317q10-208 124-322t322-124q88-5 317-5t317 5q208 10 322 124t124 322q5 88 5 317z"--}}
            {{--                                       fill="#dedede"/></svg>--}}
            {{--                       </span>--}}
            {{--                        <div class="form-group">--}}
            {{--                            <span class="link inst" data-id=""><input type="url" class="form-control" placeholder="Url"></span>--}}
            {{--                        </div>--}}
            {{--                    </li>--}}
            {{--                    <li class="item linkd">--}}
            {{--                       <span class="image">--}}
            {{--                           <svg viewBox="0 0 1792 1792" xmlns="http://www.w3.org/2000/svg"><path--}}
            {{--                                       d="M477 625v991h-330v-991h330zm21-306q1 73-50.5 122t-135.5 49h-2q-82 0-132-49t-50-122q0-74 51.5-122.5t134.5-48.5 133 48.5 51 122.5zm1166 729v568h-329v-530q0-105-40.5-164.5t-126.5-59.5q-63 0-105.5 34.5t-63.5 85.5q-11 30-11 81v553h-329q2-399 2-647t-1-296l-1-48h329v144h-2q20-32 41-56t56.5-52 87-43.5 114.5-15.5q171 0 275 113.5t104 332.5z"--}}
            {{--                                       fill="#dedede"/></svg>--}}
            {{--                       </span>--}}
            {{--                        <div class="form-group">--}}
            {{--                            <span class="link linkd" data-id=""><input type="url" class="form-control"--}}
            {{--                                                                       placeholder="Url"></span>--}}
            {{--                        </div>--}}
            {{--                    </li>--}}
            {{--                    <li class="item saveLinks">--}}
            {{--                        <button class="btn btn-primary">Save</button>--}}
            {{--                        <span class="socialErrorHead"></span>--}}
            {{--                    </li>--}}
            {{--                </ul>--}}
            {{--            </div>--}}
        </div>
        <div class="content-box notice">
            some Text For Test
        </div>
        <div class="content-box-large homeMainSection">
            <div class="panel-heading">
                <h3 align="center">{{ trans("settings.mainSection") }}</h3>
                <div class="panel-title">
                    <div class="form-group">
                        <span align="right"><span>{{trans("settings.title")}}</span>
                            @if(Request::segment(1) == "hy")
                                <input type="text" class="homeMainPostTitle form-control" data-lang="hy">
                            @elseif(Request::segment(1) == "en")
                                <input type="text" class="homeMainPostTitle form-control" data-lang="en">
                            @else
                                <input type="text" class="homeMainPostTitle form-control" data-lang="ru">
                            @endif
                        </span>
                    </div>
                </div>
                <div class="panel-title">
                    <span>{{ trans("settings.image") }}</span>
                    <div class="form-group float-md-right">
                        &nbsp;
                        <label>
                            <img src="{{asset("assets/images/file-upload.svg")}}" class="imageUploadIcon" alt="">
                            <input type="file" id="mainPostImage" name="mainPostImage" class="form-control">
                        </label>
                    </div>
                </div>
                <div class="panel-title">
                    <div class="mainPostImage">
                        <img src="" alt="">
                    </div>
                </div>
                <div class="panel-title">
                    <div class="deleteButtonblock">
                        <span class="deleteMain deleteContainer" data-id="">
                            <a href="javascript:void(0);">{{ trans('settings.delete') }}</a>
                        </span>
                    </div>
                </div>
                {{--                <div class="panel-options">--}}
                {{--                    <a href="#" data-rel="collapse"><i class="glyphicon glyphicon-refresh"></i></a>--}}
                {{--                    <a href="#" data-rel="reload"><i class="glyphicon glyphicon-cog"></i></a>--}}
                {{--                </div>--}}
            </div>
            <form action="javascript: void(0);" class="formOne" method="POST">
                <div class="panel-body">
                    {{--                    <textarea id="tinymce_full"></textarea>--}}
                    @if(Request::segment(1) == "hy")
                        <textarea id="homeEditor2_hy"></textarea>
                    @elseif(Request::segment(1) == "en")
                        <textarea id="homeEditor2_en"></textarea>
                    @else
                        <textarea id="homeEditor2_ru"></textarea>
                    @endif
                </div>
            </form>
            <div class="panel-heading">
                <div class="panel-title">
                    <h3>Services Section</h3>
                </div>
            </div>
            <div class="panel-body">
                <section class="mainNewSecInput">
                    <form action="javascript: void(0);" method="POST" id="homeServicesForm" enctype="multipart/form-data">
                        <div class="multipleContent " data-elem="first">
                            <div class="form-group ">
                                <div class="panel-title">
                                    <span>{{trans("settings.title")}}</span>
                                </div>
                                <input type="text" class="form-control serviceTitle" data-lang="multi">
                                {{--                            @if(Request::segment(1) == "hy")--}}
                                {{--                                <input type="text" class="form-control" data-lang="hy">--}}
                                {{--                            @elseif(Request::segment(1) == "en")--}}
                                {{--                                <input type="text" class="form-control" data-lang="en">--}}
                                {{--                            @else--}}
                                {{--                                <input type="text" class="form-control" data-lang="ru">--}}
                                {{--                            @endif--}}
                            </div>
                            <div class="form-group imgWay">
                                <div class="panel-title">
                                    <span>{{ trans("settings.image")  }}</span>
                                </div>
                                <label>
                                    <img src="{{asset("assets/images/file-upload.svg")}}" class="imageUploadIcon" alt="">
                                    <input type="file" class="form-control serviceImage">
                                </label>
                            </div>
                            <div class="form-group  multiply">
                                <span class="okIconContainer">
                                    <svg class="bi bi-check okIcon" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                        <path fill-rule="evenodd" d="M13.854 3.646a.5.5 0 010 .708l-7 7a.5.5 0 01-.708 0l-3.5-3.5a.5.5 0 11.708-.708L6.5 10.293l6.646-6.647a.5.5 0 01.708 0z" clip-rule="evenodd"/>
                                    </svg>
                                </span>
                                <svg class="bi bi-plus-circle multiplyIcon" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                    <path fill-rule="evenodd" d="M8 3.5a.5.5 0 01.5.5v4a.5.5 0 01-.5.5H4a.5.5 0 010-1h3.5V4a.5.5 0 01.5-.5z" clip-rule="evenodd"/>
                                    <path fill-rule="evenodd" d="M7.5 8a.5.5 0 01.5-.5h4a.5.5 0 010 1H8.5V12a.5.5 0 01-1 0V8z" clip-rule="evenodd"/>
                                    <path fill-rule="evenodd" d="M8 15A7 7 0 108 1a7 7 0 000 14zm0 1A8 8 0 108 0a8 8 0 000 16z" clip-rule="evenodd"/>
                                </svg>
                                <svg class="bi bi-trash trashIcon" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                    <path d="M5.5 5.5A.5.5 0 016 6v6a.5.5 0 01-1 0V6a.5.5 0 01.5-.5zm2.5 0a.5.5 0 01.5.5v6a.5.5 0 01-1 0V6a.5.5 0 01.5-.5zm3 .5a.5.5 0 00-1 0v6a.5.5 0 001 0V6z"/>
                                    <path fill-rule="evenodd" d="M14.5 3a1 1 0 01-1 1H13v9a2 2 0 01-2 2H5a2 2 0 01-2-2V4h-.5a1 1 0 01-1-1V2a1 1 0 011-1H6a1 1 0 011-1h2a1 1 0 011 1h3.5a1 1 0 011 1v1zM4.118 4L4 4.059V13a1 1 0 001 1h6a1 1 0 001-1V4.059L11.882 4H4.118zM2.5 3V2h11v1h-11z" clip-rule="evenodd"/>
                                </svg>

                            </div>
                        </div>
                        <div class="form-group">
                            <button type="submit" class="btn btn-primary">{{ trans("settings.save") }}</button>
                        </div>
                    </form>
                </section>
            </div>
            <div class="panel-heading">
                <div class="panel-title">
                    <h3>Our Partners</h3>
                </div>
            </div>
            <div class="pane-body">
                <section class="partnersSection">
                    <label>
                        <img src="{{asset("assets/images/file-upload.svg")}}" class="imageUploadIcon" alt="">
                        <input type="file" id="partnersImg" accept=".png, .jpg, .jpeg, .svg, .gif, .ico" multiple>
                    </label>
                    <div class="imgsPreview">
{{--                        <div class="image">--}}
{{--                            <span class="delete">--}}
{{--                                <svg viewBox="0 0 1792 1792" fill="currentColor" xmlns="http://www.w3.org/2000/svg"><path d="M1490 1322q0 40-28 68l-136 136q-28 28-68 28t-68-28l-294-294-294 294q-28 28-68 28t-68-28l-136-136q-28-28-28-68t28-68l294-294-294-294q-28-28-28-68t28-68l136-136q28-28 68-28t68 28l294 294 294-294q28-28 68-28t68 28l136 136q28 28 28 68t-28 68l-294 294 294 294q28 28 28 68z"/></svg>--}}
{{--                            </span>--}}
{{--                        </div>--}}
                    </div>
                </section>
            </div>
        </div>
        <div class="content-box-large homeFooter">
            <div class="panel-heading">
                <h3 align="center">{{ trans("settings.footerSection")  }}</h3>
            </div>
            <div class="panel-heading">
                <div class="panel-title">
                    <h4>Contact Section</h4>
                </div>
            </div>
            <div class="panel-body">
                <form method="POST" action="javascript: void(0);" id="contactFormAdmin">
{{--                    @csrf--}}

{{--                    @if($errors->any())--}}
{{--                        {!! implode('', $errors->all('<div>:message</div>')) !!}--}}
{{--                    @endif--}}

                    <div class="form-group">
                        <span>{{ trans("settings.phone") }}</span><span class="contactPhonePreview"></span>
{{--                        @error('phone')--}}
{{--                            <p color="red">--}}
{{--                                {{ $message }}--}}
{{--                            </p>--}}
{{--                        @endError--}}
                        <input type="text" class="form-control" name="phone">
                    </div>
                    <div class="form-group">
                        <span>{{ trans("settings.email") }} </span> <span class="contactEmailPreview"></span>
{{--                        @error('email')--}}
{{--                            {{ $message }}--}}
{{--                        @endError--}}
                        <input type="email" class="form-control"  name="email">
                    </div>
                    <div class="form-group">
                        <span>{{ trans("settings.address") }}</span> <span class="contactAddressPreview"></span>
                        @if(Request::segment(1) == "hy")
                            <input type="text" class="form-control"  name="address">
                        @elseif(Request::segment(1) == "en")
                            <input type="text" class="form-control" name="addressEn">
                        @else
                            <input type="text" class="form-control" name="addressRu">
                        @endif
                    </div>
                    <div class="form-group">
                        <button class="btn btn-primary" type="button" id="contactSave">{{ trans("settings.save") }}</button>
                    </div>
{{--                    @if(Session::has('contactInsertErr'))--}}
{{--                        <p>{{Session::get('contactInsertErr')}}</p>--}}
{{--                    @endif--}}
{{--                    @if(Session::has('exception'))--}}
{{--                        <p>{{Session::get('exception')}}</p>--}}
{{--                    @endif--}}
                </form>
            </div>
            {{--            <div class="panel-body">--}}
            {{--                <ul class="linksList homeFooterList">--}}
            {{--                    <li class="item fb">--}}
            {{--                        --}}{{--                       <img src="data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHZpZXdCb3g9IjAgMCAzMjAg%0D%0ANTEyIj48cGF0aCBkPSJNMjc5LjE0IDI4OGwxNC4yMi05Mi42NmgtODguOTF2LTYwLjEzYzAtMjUu%0D%0AMzUgMTIuNDItNTAuMDYgNTIuMjQtNTAuMDZoNDAuNDJWNi4yNlMyNjAuNDMgMCAyMjUuMzYgMGMt%0D%0ANzMuMjIgMC0xMjEuMDggNDQuMzgtMTIxLjA4IDEyNC43MnY3MC42MkgyMi44OVYyODhoODEuMzl2%0D%0AMjI0aDEwMC4xN1YyODh6Ii8+PC9zdmc+" alt="">--}}
            {{--                        <span class="image">--}}
            {{--                           <svg viewBox="0 0 1792 1792" xmlns="http://www.w3.org/2000/svg"><path--}}
            {{--                                       d="M1343 12v264h-157q-86 0-116 36t-30 108v189h293l-39 296h-254v759h-306v-759h-255v-296h255v-218q0-186 104-288.5t277-102.5q147 0 228 12z"--}}
            {{--                                       fill="#dedede"/></svg>--}}
            {{--                       </span>--}}
            {{--                        <div class="form-group">--}}
            {{--                            <span class="link" data-id=""><input type="url" class="form-control"--}}
            {{--                                                                 placeholder="Url"></span>--}}
            {{--                        </div>--}}
            {{--                    </li>--}}
            {{--                    <li class="item inst">--}}
            {{--                       <span class="image">--}}
            {{--                           <svg viewBox="0 0 1792 1792" xmlns="http://www.w3.org/2000/svg"><path--}}
            {{--                                       d="M1152 896q0-106-75-181t-181-75-181 75-75 181 75 181 181 75 181-75 75-181zm138 0q0 164-115 279t-279 115-279-115-115-279 115-279 279-115 279 115 115 279zm108-410q0 38-27 65t-65 27-65-27-27-65 27-65 65-27 65 27 27 65zm-502-220q-7 0-76.5-.5t-105.5 0-96.5 3-103 10-71.5 18.5q-50 20-88 58t-58 88q-11 29-18.5 71.5t-10 103-3 96.5 0 105.5.5 76.5-.5 76.5 0 105.5 3 96.5 10 103 18.5 71.5q20 50 58 88t88 58q29 11 71.5 18.5t103 10 96.5 3 105.5 0 76.5-.5 76.5.5 105.5 0 96.5-3 103-10 71.5-18.5q50-20 88-58t58-88q11-29 18.5-71.5t10-103 3-96.5 0-105.5-.5-76.5.5-76.5 0-105.5-3-96.5-10-103-18.5-71.5q-20-50-58-88t-88-58q-29-11-71.5-18.5t-103-10-96.5-3-105.5 0-76.5.5zm768 630q0 229-5 317-10 208-124 322t-322 124q-88 5-317 5t-317-5q-208-10-322-124t-124-322q-5-88-5-317t5-317q10-208 124-322t322-124q88-5 317-5t317 5q208 10 322 124t124 322q5 88 5 317z"--}}
            {{--                                       fill="#dedede"/></svg>--}}
            {{--                       </span>--}}
            {{--                        <div class="form-group">--}}
            {{--                            <span class="link" data-id=""><input type="url" class="form-control"--}}
            {{--                                                                 placeholder="Url"></span>--}}
            {{--                        </div>--}}
            {{--                    </li>--}}
            {{--                    <li class="item linkd">--}}
            {{--                       <span class="image">--}}
            {{--                           <svg viewBox="0 0 1792 1792" xmlns="http://www.w3.org/2000/svg"><path--}}
            {{--                                       d="M477 625v991h-330v-991h330zm21-306q1 73-50.5 122t-135.5 49h-2q-82 0-132-49t-50-122q0-74 51.5-122.5t134.5-48.5 133 48.5 51 122.5zm1166 729v568h-329v-530q0-105-40.5-164.5t-126.5-59.5q-63 0-105.5 34.5t-63.5 85.5q-11 30-11 81v553h-329q2-399 2-647t-1-296l-1-48h329v144h-2q20-32 41-56t56.5-52 87-43.5 114.5-15.5q171 0 275 113.5t104 332.5z"--}}
            {{--                                       fill="#dedede"/></svg>--}}
            {{--                       </span>--}}
            {{--                        <div class="form-group">--}}
            {{--                            <span class="link" data-id=""><input type="url" class="form-control"--}}
            {{--                                                                 placeholder="Url"></span>--}}
            {{--                        </div>--}}
            {{--                    </li>--}}
            {{--                    <li class="item saveLinks">--}}
            {{--                        <button class="btn btn-primary">Save</button>--}}
            {{--                        <span class="socialErrorFooter"></span>--}}
            {{--                    </li>--}}
            {{--                </ul>--}}
            {{--            </div>--}}

            <div class="panel-heading">
                <div class="panel-title">
                    <h4>Subscribe Section</h4>
                </div>
            </div>
            <div class="panel-body subscribeSection">

                <div class="subscribers">
{{--                    <span class="mail">--}}
{{--                        <span class="delete">--}}
{{--                            <svg viewBox="0 0 1792 1792" fill="currentColor" xmlns="http://www.w3.org/2000/svg"><path d="M1490 1322q0 40-28 68l-136 136q-28 28-68 28t-68-28l-294-294-294 294q-28 28-68 28t-68-28l-136-136q-28-28-28-68t28-68l294-294-294-294q-28-28-28-68t28-68l136-136q28-28 68-28t68 28l294 294 294-294q28-28 68-28t68 28l136 136q28 28 28 68t-28 68l-294 294 294 294q28 28 28 68z"/></svg>--}}
{{--                        </span>--}}
{{--                    </span>--}}
                </div>
                <form action="sendToSubscribers" id="homeSubscribersForm" method="POST" enctype="multipart/form-data">
                    @csrf
                    <div class="form-group">
                        <span class="description">{{ trans("settings.mailForSubs") }}</span>
                        <textarea name="subscriberMessage" class="form-control" id=""></textarea>
                    </div>
                    <div class="form-group">
                        <button class="btn btn-primary subscribeSubmit" type="submit">{{ trans("settings.send") }}</button>
                    </div>
                </form>
            </div>

        </div>
    </div>
    {{--    <div class="col-md-2">--}}
    {{--        <div class="content-box">--}}
    {{--            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquam fugiat minima numquam perspiciatis quidem sequi.--}}
    {{--        </div>--}}
    {{--    </div>--}}


@stop
@section("admTitle")
    {{ trans("settings.homeTitle") }}
@stop