@extends('admin.dashboard')

@section('admContent')
    <div class="col-md-8">
        <h1>Portfolio page</h1>
        <div class="content-box-large portfolioPage">
            <div class="panel-body">
                {{--                <object type="" data="https://www.php.net" width="300px" height="200px" style="overflow:auto;border:5px ridge blue;">--}}
                {{--                </object>--}}
                <form action="javascript: void(0);" method="POST">
                    <section class="portfolio">
                        <div class="form-group">
                            <div class="form-group websiteCategory">
                                <div class="panel-title">Categories</div>
                                <input type="text" class="form-control">
                            </div>
                            <div class="form-group catSaveBtn">
                                <button class="btn btn-primary" type="button">Save</button>
                            </div>
                            <ul class="portfolioCategories">
                                {{--                            <li class="item">--}}
                                {{--                                CategoryName--}}
                                {{--                                <span class="delete">--}}
                                {{--                                    <svg viewBox="0 0 1792 1792" fill="currentColor" xmlns="http://www.w3.org/2000/svg"><path d="M1490 1322q0 40-28 68l-136 136q-28 28-68 28t-68-28l-294-294-294 294q-28 28-68 28t-68-28l-136-136q-28-28-28-68t28-68l294-294-294-294q-28-28-28-68t28-68l136-136q28-28 68-28t68 28l294 294 294-294q28-28 68-28t68 28l136 136q28 28 28 68t-28 68l-294 294 294 294q28 28 28 68z"/></svg>--}}
                                {{--                                </span>--}}
                                {{--                            </li>--}}
                            </ul>
                        </div>
                        <div class="form-group websiteLink">
                            <div class="panel-title">Website Links</div>
                            <input type="text" class="form-control">
                        </div>
                        <div class="form-group linkSaveBtn">
                            <button class="btn btn-primary" type="button">Save</button>
                        </div>
                        <ul class="linksList">
{{--                            <li class="item">--}}
{{--                                <div class="linkCatBlock">--}}
{{--                                    <a href="javascript: void (0);">Link</a>--}}
{{--                                    <select name="" class="selectpicker form-control" multiple >--}}
{{--                                        <option value="">sdfg</option>--}}
{{--                                        <option value="">fgjfg</option>--}}
{{--                                        <option value="">yuiyui</option>--}}
{{--                                    </select>--}}
{{--                                </div>--}}
{{--                                <span class="delete">--}}
{{--                                    <svg viewBox="0 0 1792 1792" fill="currentColor" xmlns="http://www.w3.org/2000/svg"><path d="M1490 1322q0 40-28 68l-136 136q-28 28-68 28t-68-28l-294-294-294 294q-28 28-68 28t-68-28l-136-136q-28-28-28-68t28-68l294-294-294-294q-28-28-28-68t28-68l136-136q28-28 68-28t68 28l294 294 294-294q28-28 68-28t68 28l136 136q28 28 28 68t-28 68l-294 294 294 294q28 28 28 68z"/></svg>--}}
{{--                                </span>--}}
{{--                            </li>--}}
                        </ul>

                        {{--                        <div class="form-group">--}}
                        {{--                            <select name="" class="selectpicker form-control" id="">--}}
                        {{--                                <option value="">sdfg</option>--}}
                        {{--                                <option value="">fgjfg</option>--}}
                        {{--                                <option value="">yuiyui</option>--}}
                        {{--                            </select>--}}
                        {{--                        </div>--}}
                    </section>
                </form>
            </div>
        </div>
        <div class="content-box notice">
            some Text For Test
        </div>
    </div>
@stop

@section("admTitle")
    {{ trans("settings.portfolio") }}
@stop