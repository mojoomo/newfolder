<footer>
    <div class="myContainer">
        <div class="allRights">
            <p>All Right Reserved</p>
            <p>&copy;   2020 Touch and Take</p>
            <p>Powered by ITResources </p>
        </div>
        <div class="faqAndPrivacy">
            <a href="">FAQ</a>
            <a href="">Privacy  & Policy</a>
        </div>
        <div class="socials">
            <div>
                <a href=""><i class="fab fa-facebook-f"></i></a>
            </div>
            <div>
                <a href=""><i class="fab fa-instagram"></i></a>
            </div>
            <div>
                <a href=""><i class="fab fa-linkedin-in"></i></a>
            </div>
        </div>
    </div>
</footer>


<script type="text/javascript">
    let token = "{{ csrf_token() }}";
    let pageNameView = "{{ Request::segment(2) }}";
    let language = "{{ Request::segment(1) }}";
    let fullUrl = "{{ Request::fullUrl() }}";
    let contactMail = "{{ route('sendMail', app()->getLocale()) }}";
    let careerSearchUri = "{{ route('searchCareer', app()->getLocale()) }}";
    let portfolioSearchUri = "{{ route('searchPortfolio', app()->getLocale()) }}";
</script>

<script src="{{asset("assets/js/custom.js")}}"></script>
</body>
</html>