<footer class="adminfooter">
    <div class="container">

        <div class="copy text-center">
            &copy; Copyright 2020 <a href='#'>New Folder</a>
        </div>

    </div>
</footer>
@if(Request::segment(3) == "careers")
{{--    <script src="/assets/js/jquery1.1.js"></script>--}}
    <script src="/assets/js/jquery1.1.js"></script>
    <script src="/assets/js/jquery-1.8.3.min.js"></script>
    <script src="{{asset("assets/vendors/moment/moment.min.js")}}"></script>
    <script src="/assets/bootstrap/js/bootstrap.min.js"></script>
    <script src="{{asset("assets/js/bootstrap-datetimepicker.js")}}"></script>
    <script src="{{asset("assets/js/locales/bootstrap-datetimepicker.hy.js")}}"></script>


    {{--<script src="/assets/vendors/select/bootstrap-select.js"></script>--}}
    <script src="/assets/vendors/bootstrap-wysihtml5/lib/js/wysihtml5-0.3.0.js"></script>
    <script src="/assets/vendors/bootstrap-wysihtml5/src/bootstrap-wysihtml5.js"></script>

    <script src="/assets/vendors/ckeditor/ckeditor.js"></script>
    <script src="/assets/vendors/ckeditor/adapters/jquery.js"></script>

@else
    <script src="/assets/js/jquery1.1.js"></script>
    <script src="/assets/bootstrap/js/bootstrap.min.js"></script>


    {{--<script src="/assets/vendors/select/bootstrap-select.js"></script>--}}
    <script src="/assets/vendors/bootstrap-wysihtml5/lib/js/wysihtml5-0.3.0.js"></script>
    <script src="/assets/vendors/bootstrap-wysihtml5/src/bootstrap-wysihtml5.js"></script>

    <script src="/assets/vendors/ckeditor/ckeditor.js"></script>
    <script src="/assets/vendors/ckeditor/adapters/jquery.js"></script>
    <script src="/assets/js/editors.js"></script>
@endif



    <script type="text/javascript">
        jQuery.noConflict();
        var pageName = '{{Request::segment(3)}}';
        jQuery(document).ready(function($) {
            if(pageName !== "careers" && pageName !== "services") {
                function lockMaxLength(edt) {
                    let locked;
                    edt.on('key', function (evt) {
                        var currentLength = edt.getData().length,
                            maximumLength = 480;
                        if (currentLength >= maximumLength) {
                            if (!locked) {
                                // Record the last legal content.
                                edt.fire('saveSnapshot');
                                locked = 1;
                                // Cancel the keystroke.
                                evt.cancel();
                            } else
                            // Check after this key has effected.
                                setTimeout(function () {
                                    // Rollback the illegal one.
                                    if (edt.getData().length > maximumLength) {
                                        edt.execCommand('undo');
                                        if (edt.getData().length > maximumLength) {
                                            edt.setData(edt.getData().substring(0, maximumLength));
                                        }
                                        alert(" {{ __("settings.maxLengthPost") }} ");
                                    } else {
                                        locked = 0;
                                    }
                                }, 0);
                        }
                    });
                }

                let token = "{{ csrf_token() }}";
                /* About PAGE */
                let language = '{{ app()->getLocale() }}';
                let aboutEditor1 = $('textarea#aboutEditor1_{{Request::segment(1)}}').ckeditor({
                    language: language,
                }).editor;

                let editorAbout1 = $('textarea#aboutEditor1_{{Request::segment(1)}}');
                var aboutMainTitle = $('.aboutMainPostTitle');
                var aboutpostId;
                let aboutDelBtnHead = $(".deleteHeader.deleteContainer");



                lockMaxLength(aboutEditor1, 100);

                aboutEditor1.on("instanceReady", function() {
                    $.ajax({
                        url: "checkP",
                        method: "POST",
                        data: {"_token": token, "editorName": aboutEditor1.id, "pageName": pageName}, //+"pageAbout"
                        cache: false,
                        success: function(data) {
                            aboutpostId = data.postId;
                            aboutDelBtnHead.css({"display": "inline-block"});
                            aboutDelBtnHead.data("id", data.postId);
                            $.ajax({
                                url: "lastP",
                                method: "POST",
                                data: {"_token": token, "currentPostId": data.postId},
                                cache: false,
                                success: function(d) {
                                    if (language == 'ru') {
                                        $('textarea#aboutEditor1_{{Request::segment(1)}}').val(d.lastPost.contentRu);
                                        aboutMainTitle.data('lang', '{{Request::segment(1)}}').val(d.lastPost.nameRu);
                                    } else if (language == 'en') {
                                        $('textarea#aboutEditor1_{{Request::segment(1)}}').val(d.lastPost.contentEn);
                                        aboutMainTitle.data('lang', '{{Request::segment(1)}}').val(d.lastPost.nameEn);
                                    } else {
                                        $('textarea#aboutEditor1_{{Request::segment(1)}}').val(d.lastPost.content);
                                        aboutMainTitle.data('lang', '{{Request::segment(1)}}').val(d.lastPost.name);
                                    }
                                }
                            }).error(function(err, status, error){

                            });
                        }
                    }).error(function(err, status, error){

                    });

                    if (typeof aboutDelBtnHead != "undefined") {
                        aboutDelBtnHead.on("click touchstart", function (ev) {
                            ev.preventDefault();
                            ev.stopPropagation();

                            $.ajax({
                                url: "deletePost",
                                method: "POST",
                                data: {
                                    "_token": token,
                                    "delId": aboutDelBtnHead.data('id')
                                },
                                cache: false,
                                success: function (d) {
                                    console.log(d)
                                }
                            }).error(function (err, status, error) {
                                console.log(error);
                            });
                        });
                    }
                });

                aboutMainTitle.on('input', function () {
                    aboutMainTitle.css({"border": ""});
                });

                aboutEditor1.on("afterCommandExec", function (event) {
                    var commandName = event.data.name;
                    let data = $('textarea#aboutEditor1_{{Request::segment(1)}}').val();
                    let aboutPostHeadUrl = aboutpostId > 0 && typeof aboutpostId != "undefined" && typeof aboutpostId != "null" ? '/{{ app()->getLocale() }}/admin/pUpdate' : '/{{ app()->getLocale() }}/admin/pSave';
                    if (aboutMainTitle.val() == "") {
                        aboutMainTitle.css({"border": "1px solid red"});
                        $(".content-box.notice").addClass("show");
                        $(".content-box.notice.show").css({"border-color":"red", "color":"red"});
                        $(".content-box.notice").text("{{trans("settings.error_message") }}");
                        setTimeout(function() {
                            $(".content-box.notice").removeClass("show");
                        }, 6000);
                        return false;
                    }

                    if (editorAbout1.val() == "") {
                        $(".content-box.notice").addClass("show");
                        $(".content-box.notice.show").css({"border-color":"red", "color":"red"});
                        $(".content-box.notice").text("{{trans("settings.error_message") }}");
                        setTimeout(function() {
                            $(".content-box.notice").removeClass("show");
                        }, 6000);
                        return false;
                    }

                    if(commandName == "save") {
                        $.ajax({
                            url: aboutPostHeadUrl,
                            method: "POST",
                            data: {"_token": token,
                                "contentText": data,
                                "title": aboutMainTitle.val(),
                                "pageName": pageName,
                                "lang": language,
                                "categoryName": "main_content",
                                "img": "",
                                "typeName": aboutEditor1.id, //+"pageAbout"
                                "pId": aboutpostId
                            },
                            cache: false,
                            success: function(data) {
                                // console.log(data);
                                $(".content-box.notice").addClass("show");
                                $(".content-box.notice.show").css({"border-color":"green", "color":"green"});
                                $(".content-box.notice").text("{{trans("settings.success_save") }}");
                                setTimeout(function() {
                                    $(".content-box.notice").removeClass("show");
                                }, 6000);
                            }
                        }).error(function(err, status, error) {

                        });
                    }

                });
            }

            let logoutUri = "{{ route('logout', ['language' => app()->getLocale()])  }}";
            $(".adminLogoutBtn").on("click touchstart", function (e) {
                $.ajax({
                    url: logoutUri,
                    type: "POST",
                    data: {"_token": "{{ csrf_token() }}"},
                    cache: false,
                    success: function (d) {
                        {{--location.href = "{{ route('/', ['language' => app()->getLocale()]) }}"--}}
                        location.reload();
                    }
                }).error(function (err) {
                    console.log(err);
                })

            });
        });

        let language = "{{ Request::segment(1) }}";
        let token = "{{ csrf_token()  }}";
        let successSaveMessage = "{{ trans('settings.success_save') }}";
        let qualificationsTrans = "{{ trans('settings.qualifications')  }}";
        let contentTrans = "{{ trans('settings.content')  }}";
        let dateTrans = "{{ trans('settings.date')  }}";
        let locationTrans = "{{ trans('settings.location')  }}";
        let titleTrans = "{{ trans('settings.title')  }}";
        let saveTrans = "{{ trans('settings.save') }}";

</script>
<script src="/assets/js/custom.js"></script>
</body>
</html>