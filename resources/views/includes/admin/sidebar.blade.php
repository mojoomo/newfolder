<div class="col-md-2">
    <div class="sidebar content-box" style="display: block;">
        <ul class="nav">
            <!-- Main menu -->
            <li class="current"><a href="panel"><i class="glyphicon glyphicon-calendar"></i> Dashboard</a></li>
            <li><a href="home"><i class="glyphicon glyphicon-home"></i>{{ trans('settings.home') }}</a></li>
            <li><a href="about"><i class="glyphicon glyphicon-education"></i>{{ trans('settings.aboutUs') }}</a></li>
            <li><a href="services"><i class="glyphicon glyphicon-briefcase"></i>{{ trans('settings.services') }}</a></li>
            <li><a href="portfolio"><i class="glyphicon glyphicon-calendar"></i>{{ trans('settings.portfolio') }}</a></li>
            <li><a href="careers"><i class="glyphicon glyphicon-signal"></i>{{ trans('settings.career') }}</a></li>
{{--            <li class="submenu">--}}
{{--                <a href="#">--}}
{{--                    <i class="glyphicon glyphicon-list"></i> Pages--}}
{{--                    <span class="caret pull-right"></span>--}}
{{--                </a>--}}
{{--                <!-- Sub menu -->--}}
{{--                <ul>--}}
{{--                    <li><a href="login.html">Login</a></li>--}}
{{--                    <li><a href="/logout">Signup</a></li>--}}
{{--                </ul>--}}
{{--            </li>--}}
        </ul>
    </div>
</div>