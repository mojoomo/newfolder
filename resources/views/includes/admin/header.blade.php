<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=0, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <!-- Bootstrap -->
    <link href="/assets/bootstrap/css/bootstrap.css" rel="stylesheet">

{{--    <link href="/assets/vendors/select/bootstrap-select.css" rel="stylesheet">--}}
    <link href="/assets/bootstrap/css/bootstrap-select.css" rel="stylesheet">

    <!-- styles -->
    <link href="/assets/css/styles.css" rel="stylesheet">
    <link href="{{ asset("assets/css/bootstrap-datetimepicker.css")}}" rel="stylesheet">
    <link href="/assets/css/custom.css" rel="stylesheet">

    <title> @if(Request::segment(3) == "panel") {{ trans("settings.dashboard") }} @else @yield('admTitle') @endif </title>
    <style>
        .wrapblock .row {
            margin: auto 0;
        }
        .langNav ul, .langNav ul li {
            width: 100%;
        }
        .langNav ul li span {
            color: snow;
        }
        .langNav ul li a {
            display: inline-block;
        }
        .header .navbar.langNav .nav > li a:hover {
            color: #2c3742 !important;
        }
        .header .navbar.langNav .nav > li a:focus {
            color: #2c3742 !important;
        }

        .active {
            background-color: #eee;
            color: #2c3742 !important;
        }

    </style>

</head>
<body>
