<div class="portfolioItems">

    @if(isset($posts))
        @foreach($posts as $post)
            <div class="item" data-id="{{ $post->id }}">
                <?php

                    //website url
                     $siteURL = "$post->link";

                     //call Google PageSpeed Insights API
                     $googlePagespeedData = @file_get_contents("https://www.googleapis.com/pagespeedonline/v2/runPagespeed?url=$siteURL&screenshot=true");

                     //decode json data
                     $googlePagespeedData = json_decode($googlePagespeedData, true);

                     //screenshot data
                     $screenshot = $googlePagespeedData['screenshot']['data'];
                     $screenshot = str_replace(array('_','-'),array('/','+'),$screenshot);

                ?>
                <div class="logoAndLink">

{{--                                                <img src="{{asset('assets/images/portfolio/white.png')}}" alt="">--}}
                    <img src="{{ $screenshot != "" ? "data:image/jpeg;base64,$screenshot" : asset('assets/images/portfolio/white.png') }}">
{{--                    <iframe style="width:100%; height: 100%;" src="{{ $post->link }}"></iframe>--}}
                    <a href="{{ $post->link }}" target="_blank">
                        <span>{{ trans("settings.viewWebsite") }}</span>
                        <span>
			 	 					<i class="fas fa-chevron-right"></i>
			 	 				</span>
                    </a>
                </div>
                <div class="itemImageBackground" style="background-image: url({{ asset("assets/images/portfolio/ITResources.png") }});">

                </div>
            </div>
        @endforeach
    @endif
    @if(isset($postByCat))
{{--        {{ dd($postByCat) }}--}}
        @foreach($postByCat as $p)
            <div class="item" data-id="{{ $p->id }}">
                <div class="logoAndLink">
                    <?php

                        //website url
                         $siteURL = "$p->link";

                         //call Google PageSpeed Insights API
                         $googlePagespeedData = @file_get_contents("https://www.googleapis.com/pagespeedonline/v2/runPagespeed?url=$siteURL&screenshot=true");

                         //decode json data
                         $googlePagespeedData = json_decode($googlePagespeedData, true);

                         //screenshot data
                         $screenshot = $googlePagespeedData['screenshot']['data'];
                         $screenshot = str_replace(array('_','-'),array('/','+'),$screenshot);

                        //display screenshot image
//                         echo "<img src=\"data:image/jpeg;base64,".$screenshot."\" />";
                    ?>
{{--                    <img src="{{asset('assets/images/portfolio/white.png')}}" alt="">--}}
                        <img src="{{ $screenshot != "" ? "data:image/jpeg;base64,$screenshot" : asset('assets/images/portfolio/white.png') }}">
{{--                   <!-- <iframe style="width:100%; height: 100%;" src="{{ $p->link }}"></iframe> --> --}}
                    <a href="{{ $p->link }}" target="_blank">
                        <span>{{ trans("settings.viewWebsite") }}</span>
                        <span>
        			 	 					<i class="fas fa-chevron-right"></i>
        			 	 				</span>
                    </a>
                </div>
                <div class="itemImageBackground"
                     style="background-image: url({{ asset("assets/images/portfolio/ITResources.png") }});">

                </div>
            </div>
        @endforeach
    @endif
        {{--    @if(isset($ok))--}}
{{--            @foreach($ok as $cat)--}}
{{--                @if(isset($cat->posts))--}}
{{--                    @foreach($cat->posts as $p)--}}
{{--                        <div class="item" data-id="{{ $p->id }}">--}}
{{--                            <div class="logoAndLink">--}}
{{--                                --}}{{--                            <img src="{{asset('assets/images/portfolio/white.png')}}" alt="">--}}
{{--                                <iframe style="width:100%; height: 100%;" src="{{ $p->link }}"></iframe>--}}
{{--                                <a href="{{ $p->link }}" target="_blank">--}}
{{--                                    <span>View Website</span>--}}
{{--                                    <span>--}}
{{--			 	 					<i class="fas fa-chevron-right"></i>--}}
{{--			 	 				</span>--}}
{{--                                </a>--}}
{{--                            </div>--}}
{{--                            <div class="itemImageBackground" style="background-image: url({{ asset("assets/images/portfolio/ITResources.png") }});">--}}

{{--                            </div>--}}
{{--                        </div>--}}
{{--                    @endforeach--}}
{{--                @endif--}}
{{--            @endforeach--}}
{{--    @endif--}}

    {{--                <div class="item">--}}
    {{--                    <div class="logoAndLink">--}}
    {{--                        <img src="img/portfolio/white.png" alt="">--}}
    {{--                        <a href="">--}}
    {{--                            <span>View Website</span>--}}
    {{--                            <span>--}}
    {{--			 	 					<i class="fas fa-chevron-right"></i>--}}
    {{--			 	 				</span>--}}
    {{--                        </a>--}}
    {{--                    </div>--}}
    {{--                    <div class="itemImageBackground" style="background-image: url(img/portfolio/ITResources.png);">--}}

    {{--                    </div>--}}
    {{--                </div>--}}
    {{--                <div class="item">--}}
    {{--                    <div class="logoAndLink">--}}
    {{--                        <img src="img/portfolio/white.png" alt="">--}}
    {{--                        <a href="">--}}
    {{--                            <span>View Website</span>--}}
    {{--                            <span>--}}
    {{--			 	 					<i class="fas fa-chevron-right"></i>--}}
    {{--			 	 				</span>--}}
    {{--                        </a>--}}
    {{--                    </div>--}}
    {{--                    <div class="itemImageBackground" style="background-image: url(img/portfolio/ITResources.png);">--}}

    {{--                    </div>--}}
    {{--                </div>--}}

</div>
<div class="pagination">
    @if(isset($posts))
        {{ $posts->links() }}

    @else


        {{ $postByCat->links() }}
        {{--        {{dd($ok->links())}}--}}
    @endif
    {{--                <button class="nextPrev">Next</button>--}}
    {{--                <button class="active">1</button>--}}
    {{--                <button>2</button>--}}
    {{--                <button>3</button>--}}
    {{--                <button>4</button>--}}
    {{--                <button class="nextPrev">Prev.</button>--}}
</div>