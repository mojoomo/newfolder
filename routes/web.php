<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::redirect('/', 'hy');
Route::prefix('{language}')->group(function () {

    Route::get('/', 'PageController@home')->name('home');
    Route::get('/admin', 'PageController@admin')->name('admin');
    Route::post('/sendMail', 'MailController@contactData')->name('sendMail');
    Route::post('/subscribe', 'MailController@subscription');
    // Page about
    Route::get('/about', 'PageController@about')->name('about');
    // Page services
    Route::get('/services', 'PageController@services')->name('services');
    Route::post('/services', 'OrderController@storeOrder');

    // Page Portfolio
    Route::get('/portfolio', 'PageController@portfolio')->name('portfolio');
    Route::post('/portfolio', 'PageController@portfolio');
    Route::post('/portfolioFilter', 'PortfolioController@filterPortfolio');
    Route::get('/searchPortfolio', 'PortfolioController@filterPortfolio')->name("searchPortfolio");

    // Page Careers
    Route::get('/careers', 'PageController@careers')->name('careers');
    Route::post('/careers', "MailController@careerMail");
    Route::get('/searchCareer', "CareerController@search")->name("searchCareer");


    Auth::routes();

    Route::prefix('admin')->middleware('auth')->group(function () {

        Route::get('/panel', 'AdminController@panel')->name('panel');
        // Home page
        Route::get('/home', 'AdminController@home')->name('admHome');
        Route::get('/logout', 'AdminController@logout');
        Route::post('/pSave', 'PostController@save');
        Route::post('/pUpdate', 'PostController@update');
        Route::post('/lastP', 'PostController@getLast');
        Route::post('/checkP', 'PostController@checkPost');
        Route::post('/checkList', 'PostController@checkList');
//        Route::post('/addLinks', 'PostController@socialLinksAdd');
//        Route::post('/updateLinks', 'PostController@socialUpdate');
        Route::post('/deletePost', 'PostController@remove');
//        Route::post('/deleteSocLink', 'PostController@removeSocLink');
//        Route::post('/getSocialError', 'ErrorController@showSocialLinksError');
        Route::post('/uploadLogo', 'UploadController@logoUpload');
        Route::post('/saveServices', 'PostController@addServices');
        Route::post('/updateServices', 'PostController@editServices');
        Route::post('/saveImages', 'UploadController@imageListSave');
        Route::post('/addContactData', 'ContactController@store');
        Route::post('/checkContacts', 'ContactController@show');
        Route::post('/updateContactData', 'ContactController@update');
        Route::post('/getSubscribers', "MailController@getSubscribers");
        Route::delete('/deletable', 'MailController@deletable');
        Route::post('/sendToSubscribers', 'MailController@sendToSubscribers');
        // About Page
        Route::get('/about', 'AdminController@about')->name("admAbout");

        // Services Page
        Route::get('/services', 'AdminController@services')->name("admServices");
        Route::post('/removeApplicant', 'OrderController@removeApplicant');


        //Portfolio Page
        Route::get('/portfolio', 'AdminController@portfolio')->name("admPortfolio");
        Route::post('/savePortfolioCategory', 'PortfolioController@saveCategory');
        Route::post('/delCategory', 'PortfolioController@destroyCategory');
        Route::post('/checkDataPortfolio', 'PortfolioController@check');
        Route::post('/createPortfolioLink', 'PortfolioController@saveLinks');
        Route::post('/delLink', 'PortfolioController@destroyLink');
        Route::post('/setRelationsPortfolio', 'PortfolioController@storeRelation');

        // Page Career
        Route::get('/careers', 'AdminController@career')->name("admCareer");
        Route::post('/storeCareer', 'CareerController@store');
        Route::post('/showCareer', 'CareerController@show');
        Route::post('/destroyCareer', 'CareerController@destroy');
        Route::post('/updateCareer', 'CareerController@edit');
        Route::post('/delCareerCat', 'CareerController@destroyCat');
//        Route::post('/careerMail', "MailController@test");

    });

});




