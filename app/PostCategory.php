<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
//use App\Post;
//use App\Category;
//use Illuminate\Database\Eloquent\Relations\Pivot;

class PostCategory extends Model
{
    //
    protected $fillable = ['postId', 'catId'];
    protected $table = 'postcategory';

//    public function post() {
//        return $this->belongsTo('App\Post');
//    }
//
//    public function category() {
//        return $this->belongsTo('App\Category');
//    }
}
