<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Relation extends Model
{
    //
    protected $fillable = ['categId','careerId','created_at','updated_at'];
    protected $table = 'relation';
    public $timestamps = true;
}
