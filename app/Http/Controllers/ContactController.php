<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use App\Contact;
use Illuminate\Http\Request;
use Validator;
use Exception;
use Carbon\Carbon;

class ContactController extends Controller
{

    public function __construct() {
        $this->middleware("auth");
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        try {

           if($request->ajax()) {

               $validationArr = array();
               $validationArr["phoneContact"] = 'required|regex:/^(\(?\+?[0-9]*\)?)?[0-9_\s+\(\)]*$/';
               $validationArr["emailContact"] = "required|email|max:255";

               if(isset($request->address)) {
                   $validationArr["addressContact"] = "required|regex:/[a-zA-Z0-9\s]+/";
               }else if(isset($request->addressEn)) {
                   $validationArr["addressEnContact"] = "required|regex:/[a-zA-Z0-9\s]+/";
               }
               if(isset($request->addressRu)) {
                   $validationArr["addressRuContact"] = "required|regex:/[a-zA-Z0-9\s]+/";
               }

               $hasErr = Validator::make($request->all(), $validationArr);

               if($hasErr->fails()) {
                   return response()->json(["contactInsertErr" => __("settings.contactErr"), "allErr" => $hasErr->messages()], 422);
               }else {
//                Contact::query()->delete();

                   $insertArr = array();
                   $insertArr["phone"] = $request->phoneContact;
                   $insertArr["email"] = $request->emailContact;
                   $insertArr["address"] = $request->addressContact;
                   $insertArr["addressEn"] = $request->addressEnContact;
                   $insertArr["addressRu"] = $request->addressRuContact;
                   $insertArr["created_at"] = Carbon::now();
                   $insertArr["updated_at"] = Carbon::now();
                   Contact::query()->delete();
                   $res = Contact::create($insertArr);
                   return response()->json(["success" => __('settings.successMessage'), "record" => $res], 200);
               }
           }

        }catch(Exception $e) {
            return back()->with("exception", $e);
//            dd($e);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Contact  $contact
     * @param  Request  $request
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
        if($request->ajax()) {
            $all = Contact::all();
            if(count($all) > 0)
                return response()->json(["record" => $all], 200);
            else
                return response()->json(["fail" => $all], 422);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Contact  $contact
     * @return \Illuminate\Http\Response
     */
    public function edit(Contact $contact)
    {
        //
    }


    public function update(Request $request)
    {
        if($request->ajax()) {
//            if(Contact::select('id')->first() != null) {
//                $latestPost = Contact::select('id')->orderBy('id', 'DESC')->first()->id;
//            }else {
//                $latestPost = 0;
//            }
            $updArr = array();
            $validationArr = array();
            $validationArr["phoneContact"] = 'required|regex:/^(\(?\+?[0-9]*\)?)?[0-9_\s+\(\)]*$/';
            $validationArr["emailContact"] = "required|email|max:255";
            $updArr["phone"] = $request->phoneContact;
            $updArr["email"] = $request->emailContact;

            if(isset($request->addressContact)) {
                $updArr["address"] = $request->addressContact;
            }elseif(isset($request->addressEnContact)) {
                $updArr["addressEn"] = $request->addressEnContact;
            }elseif(isset($request->addressRuContact)){
                $updArr["addressRu"] = $request->addressRuContact;
            }

            $hasErr = Validator::make($request->all(), $validationArr);

            if($hasErr->fails()) {
                return response()->json(["contactInsertErr" => __("settings.contactErr"), "allErr" => $hasErr->messages()], 422);
            }else {
                $updated = Contact::find($request->contactId)->update($updArr);
                if($updated) {
                    $insertedRecord = Contact::find($request->contactId)->get();
                    return response()->json(["record" => $insertedRecord], 200);
                }
            }


        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Contact  $contact
     * @return \Illuminate\Http\Response
     */
    public function destroy(Contact $contact, Request $request)
    {
        //

    }
}
