<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\PostController as P;
use App\Post;
use App\Order;

class AdminController extends Controller
{

    public function __construct() {
        $this->middleware('auth');
    }

    public function panel(P $p) {
        return view('admin.dashboard');
    }


    public function home(P $p)  {
        $logoPath = Post::where('type','mainLogo')->get('image');
        return view('admin.home', compact("logoPath"));
    }

    public function about() {
        return view("admin.about");
    }
    public function career() {
        return view("admin.careers");
    }
    public function services() {
        $orders = Order::all();
        return view("admin.services", compact('orders'));
    }

    public function portfolio() {
        return view("admin.portfolio");
    }



}
