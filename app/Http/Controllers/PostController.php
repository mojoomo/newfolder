<?php

namespace App\Http\Controllers;

use App\Post;
use App\Category;
use Carbon\Carbon;
use App\PostCategory;
use Illuminate\Http\Request;
use Mavinoo\LaravelBatch\Batch;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Session;
use Exception;


class PostController extends Controller
{
    //

    public function __construct() {
        $this->middleware('auth');
    }

    // get category by post Id
    public function catByPostId($id) {
        $byPost = Post::find($id);
        return $byPost->category;
    }

    // get Latest Post
    public function getLast(Request $request) {

        $lastPost = Post::byId($request->currentPostId);

        if($lastPost !== null) {
           return response()->json(['lastPost' => $lastPost, 'categories' =>  $this->catByPostId($request->currentPostId)]);
        }else {
           return response()->json(['lastPost' => "fail" ]);
        }
    }

    public function getByPostId(Request $request) {
        return Post::byId($request->Id);
    }
//    public function test() {
//        dd(Session::get('postCatId'));
//    }
    public function postCatUpdate($catId, $postId, $pCatId) {
        PostCategory::where('id', Session::has('postCatId') ? Session::get('postCatId') : $pCatId)->update(['postId' => $postId, 'catId' => $catId]);
        return true;
    }

    public function postCatSave($catId, $postId) {
        $postCat = PostCategory::create(['postId' => $postId, 'catId' => $catId]);
        Session::put('postCatId', $postCat->id);
        return $postCat;
    }

    public function save(Request $request) {
        if($request->ajax()) {
            $postArr = [];
            $postArr['userId'] = Auth::user()->id;
            $postArr['type'] = $request->typeName;
            $postArr['page'] = $request->pageName;

            if(isset($request->image)) {
                $imageName = $request->typeName . 'img.' . $request->image->extension();
                $request->image->move(public_path('assets/images/upload'), $imageName);
                $img = $request->getBaseUrl() . '/assets/images/upload/' . $imageName;
                $postArr['image'] = $img;
            }

            if($request->lang == 'hy') {
                $postArr['content'] = $request->contentText;
                $postArr['name'] = $request->title;
            }else if($request->lang == 'en') {
                $postArr['contentEn'] = $request->contentText;
                $postArr['nameEn'] = $request->title;
            }else {
                $postArr['contentRu'] = $request->contentText;
                $postArr['nameRu'] = $request->title;
            }

            $post = Post::create($postArr);
            $cat = Category::create(['name' => $request->categoryName]);
            $postCat = $this->postCatSave($cat->id, $post->id);
            if($post && $cat && $postCat) {
                Session::put('savedCatId', $cat->id);
                return response()->json(['lastId' => $post->id,'catName' => $cat->name, "ImgPath" => isset($img) ? $img : ""]);
            }else {
                return response("fail");
            }
        }

    }

    public function update(Request $request, Post $post) {
        try{
            if($request->ajax()) {
                if($request->pId > 0) {
                    $id = $request->pId;
                    $updateArr = [];
                    if($request->lang == 'hy') {
                        $updateArr['content'] = $request->contentText;
                        $updateArr['name'] = $request->title;
                    }else if($request->lang == 'en') {
                        $updateArr['contentEn'] = $request->contentText;
                        $updateArr['nameEn'] = $request->title;
                    }else if($request->lang == 'ru') {
                        $updateArr['contentRu'] = $request->contentText;
                        $updateArr['nameRu'] = $request->title;
                    }

                    $updateArr['type'] = $request->typeName;
                    $updateArr['page'] = $request->pageName;

                    if(isset($request->image)) {
                        $imageName = $request->typeName . 'img.' . $request->image->extension();
                        $request->image->move(public_path('assets/images/upload'), $imageName);
                        $img = $request->getBaseUrl() . '/assets/images/upload/' . $imageName;
                        $updateArr['image'] = $img;
                    }

//                    $catPId = PostCategory::where("postId", $id)->first();
//                    $cId = Session::has('savedCatId') ? Session::get('savedCatId') : (isset($catPId) ? $catPId->catId : null);
//                    $updateCat = Category::where('id', isset($cId) && $cId > 0 ? $cId : $cId->catId)->update(['name' => $request->categoryName]);
                    $exists = $post::where('id', '=', $id)->update($updateArr);
//                    $postCat = $this->postCatUpdate(isset($cId->catId) ? $cId->catId : $cId,  isset($id) ? $id : null, isset($cId->id) ? $cId->id : (isset($catPId->id) ? $catPId->id : null));
//                    dd($exists .",". $updateCat .",". $postCat);
                    if($exists ) { //&& $updateCat && $postCat
                        return response()->json(['lastId' => $id]);
                    }else {
                        return response('filed', 422);
                    }
                }
            }
        }catch(Exception $e) {
            dd($e);
        }
    }

//    public function socialUpdate(Request $request, Batch $batch) {
//        try{
//            if($request->ajax()) {
//                $userId  = Auth::user()->id;
//                $postInstance = new Post;
//
//                $updateLinksId = [$request->socialLinksData['fbId'], $request->socialLinksData['instId'], $request->socialLinksData['linkdId']];
//
//                $updateLinks = [
//                    [
//                        "id"  =>  $request->socialLinksData['fbId'],
//                        "userId" => $userId,
//                        "name"=> $request->socialLinksData['fbLink'],
//                        "page" => $request->socialLinksData['pageName'],
//                        "type" => $request->socialLinksData['type'],
//                    ],
//                    [
//                        "id"  =>  $request->socialLinksData['instId'],
//                        "userId" => $userId,
//                        "name"=> $request->socialLinksData['instLink'],
//                        "page" => $request->socialLinksData['pageName'],
//                        "type" => $request->socialLinksData['type'],
//                    ],
//                    [
//                        "id"  => $request->socialLinksData['linkdId'],
//                        "userId" => $userId,
//                        "name"=> $request->socialLinksData['linkdLink'],
//                        "page" => $request->socialLinksData['pageName'],
//                        "type" => $request->socialLinksData['type'],
//                    ]
//                ];
////            $batchSize = 500;  https://packagist.org/packages/mavinoo/laravel-batch
//                $upd = $batch->update($postInstance, $updateLinks, "id");
//                $links = Post::select(['id', 'name', 'type'])->whereIn('id', $updateLinksId)->get();
//                if($upd != false) {
//                    return response()->json(["linksId"=>$links]);
//                }else {
//                    return response('fail', 422);
//                }
//
//            }
//        } catch(\Exception $e){
//            dd($e);
//        }
//    }

//    public function socialLinksAdd(Request $request, Post $p) {
//        if($request->ajax()) {
//            $userId = Auth::user()->id;
//            Post::where("type", $request->socialLinksData['type'])->delete();
//            if(Post::select('id')->first() != null) {
//                $latestPost = Post::select('id')->orderBy('id', 'DESC')->first()->id;
//            }else {
//                $latestPost = 0;
//            }
//
//            $linksData = [
//                [
//                    "userId" => $userId,
//                    "name" => $request->socialLinksData['fbLink'], "content" => $request->socialLinksData['fbIcon'],
////                    "nameRu" => $request->linksData->fbLink, "contentRu" => $request->linksData->fbIcon,
////                    "nameEn" => $request->linksData->fbLink, "contentEn" => $request->linksData->fbIcon,
//                    "page" => $request->socialLinksData['pageName'],
//                    "type" => $request->socialLinksData['type'],
//                    "created_at" => Carbon::now(),//date('Y-m-d H:i:s'),
//                    "updated_at" => Carbon::now(),
//                ],
//                [
//                    "userId" => $userId,
//                    "name" => $request->socialLinksData['instLink'], "content" => $request->socialLinksData['instIcon'],
//                    "page" => $request->socialLinksData['pageName'],
//                    "type" => $request->socialLinksData['type'],
//                    "created_at" => Carbon::now(),
//                    "updated_at" => Carbon::now(),
//                ],
//                [
//                    "userId" => $userId,
//                    "name" => $request->socialLinksData['linkdLink'], "content" => $request->socialLinksData['linkdIcon'],
//                    "page" => $request->socialLinksData['pageName'],
//                    "type" => $request->socialLinksData['type'],
//                    "created_at" => Carbon::now(),
//                    "updated_at" => Carbon::now(),
//                ]
//            ];
//
//            $links = Post::insert($linksData);
//
//            if($links) {
//
//                $insertedIds = Post::select(['id', 'name', 'type'])->where('id', '>', $latestPost)->get();
//
//                return response()->json(['linkInfo' => $insertedIds]);
//            }
//        }
//    }


    public function addServices(Request $request, Batch $batch) {
        if($request->ajax()) {
            if(Post::select('id')->first() != null) {
                $latestPost = Post::select('id')->orderBy('id', 'DESC')->first()->id;
            }else {
                $latestPost = 0;
            }
            $all = $request->all();
            $hasPost = Post::where("type", $request->type)->count();
            if($request->dataInsert == true && $hasPost > 0) {
                Post::where("type", $request->type)->delete();
            }
            $txtCount = [];
            $imgCount = [];
            foreach($all as $key => $value ) {

                if(strpos($key, "text") !== false) {
                    $txtCount[] = $key;
                }
//                if(strpos($key, "img") !== false) {
//                    $imgCount[] = $key;
//                }
            }
            $len =  intval(floor( abs(count($txtCount))));

            $instance = new Post;
            $columns = [
                'userId',
                'type',
                'page',
                'name',
                'image',
                'created_at',
                'updated_at'
            ];
            $values = array();
            $arrInLoop = array();


            for($i = 0; $i < $len; $i++) {
                $arrInLoop["userId"] = Auth::user()->id;
                $arrInLoop["type"] = $request->type;
                $arrInLoop["page"] = $request->page;
                $reqImg = 'img$i';
                eval("\$reqImg = \"$reqImg\";");
                if(isset($all["text".$i]) && !empty($all["text".$i])) {
                    $arrInLoop["name"] = $all["text".$i];
                }else {
                    $arrInLoop["name"] = NULL;
                }
                if(isset($all["img".$i])) {
                    $imageName = "service".$i . '.' . $request->$reqImg->extension();
                    $request->$reqImg->move(public_path('assets/images/upload'), $imageName);
                    $img = $request->getBaseUrl() . '/assets/images/upload/' . $imageName;
                    $arrInLoop["image"] = $img;
                }else {
                    $arrInLoop["image"] = NULL;
                }
                $arrInLoop["created_at"] = Carbon::now();
                $arrInLoop["updated_at"] = Carbon::now();
                $values[] = $arrInLoop;
            }
            $size = 500;
            $result = $batch->insert($instance, $columns, $values, $size);
            if($result) {
                $insertedIds = Post::select(['id', 'name', 'image', 'type'])->where('id', '>', $latestPost)->get();
                return response()->json(['success' => true,'serviceIds' => $insertedIds ], 200);
            }else {
                return response('fail', 422);
            }
        }
    }

    public function editServices(Request $request, Batch $batch) {
        if($request->ajax()) {
            $ids = json_decode($request->ids);
            $updArr = array();
            $values = array();
            $all = $request->all();
            $instance = new Post;
            if(count($ids) > 0) {

                foreach($ids as $count => $val) {
                    $img = 'img$count';
                    $txt = 'text$count';
                    eval("\$img = \"$img\";");
                    eval("\$txt = \"$txt\";");
                    $updArr["id"] = $val;
                    $updArr["userId"] = Auth::user()->id;
                    if(isset($all["text".$count]) && !empty($all["text".$count])) {
                        $updArr["name"] = $request->$txt;
                    }else {
                        $updArr["name"] = NULL;
                    }
//                    $postHasImage = Post::where('image', $image)->count();
                    $pathImg = Post::find($val);
                    if(isset($all['img'.$count])) {
                        $myImg = strstr(substr(strrchr($pathImg->image, "/"), 1), ".", true);
                        $imageName = $myImg . '.' . $request->$img->extension();
                        $request->$img->move(public_path('assets/images/upload'), $imageName);
                        $image = $request->getBaseUrl() . '/assets/images/upload/' . $imageName;
                        $updArr["image"] = $image;
                    }else {
                        $updArr["image"] = $pathImg->image;
                    }

                    $values[] = $updArr;
//                    unset($updArr);
                }

//                dd($values);
                $upd = $batch->update($instance, $values, "id");

                if($upd) {
                    $sIds = Post::select(['id', 'name', 'type'])->whereIn('id', $ids)->get();
                    return response()->json(["success"=> true, "serviceIds" => $sIds], 200);
                }else {
                    return response("fail", 422);
                }
            }
        }
    }

//    public function removeSocLink(Request $request) {
//        if($request->ajax()) {
//            $query = Post::whereIn('id', $request->itemsId)->delete();
//            if($query !=  null) {
//                return response("Ok", 200);
//            }else {
//                return response("fail", 422);
//            }
//        }
//    }

//    public function getAll() {
//        return Post::allPosts();
//    }





    public function checkList(Request $request) {
        if($request->ajax()) {
            $checkL = Post::select(['id', 'name','image','type'])->where('type', $request->type)->get();
            if($checkL !== null) {
                $idsArr = [];
                foreach($checkL as $L) {
                    $idsArr[] = $L;
                }
                if(empty($idsArr))
                    return response("fail", 422);
                return response()->json($idsArr, 200);
            }else {
                return response("Fail", 422);
            }
        }
    }

    public function checkPost(Request $request) {
        if($request->ajax()) {
            $existPost = Post::checkByTypeAndPage($request->editorName, $request->pageName);
            if( $existPost != null && $request->editorName !== "undefined" && $request->pageName !== "undefined") {
                return response()->json(['postId' => $existPost->id]);
            }else {
                return response('fail', 422);
            }
        }
    }

    public function remove(Request $request) {
        if($request->ajax()) {
           $current = Post::find($request->delId);
           if(isset($current->image) && $current->image != "") {
               $imagePath = str_replace( '/', '\\', $current->image);
               File::Delete(public_path().$imagePath);
           }
           $current->delete();
           return response("Ok", 200);
        }
    }


}
