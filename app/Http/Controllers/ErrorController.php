<?php

namespace App\Http\Controllers;
use Validator;
use Exception;
use Illuminate\Http\Request;

class ErrorController extends Controller
{
    //
    public function showSocialLinksError(Request $request) {
        try{
            if($request->ajax()) {
                $errorSocial = Validator::make($request->all(), [
                    'linkFb' => "required|min:1",
                    'linkInst' => "required|min:1",
                    'linkLinkd' => "required|min:1",
                ]);

                if($errorSocial->fails()) {
                    return response()->json(["socialError" => __('settings.socialError')]);
                }
            }
        }catch(\Exception $e) {
//            abort(404);
            dd($e);
        }
    }

}
