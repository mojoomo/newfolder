<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Contact extends Model
{
    //
    protected $fillable = ['phone', 'email', 'address'];
    protected $table = 'contacts';
    public $timestamps = true;

    public static function getFirst() {
        return parent::all()->first();
    }
}
