jQuery.noConflict();
jQuery(document).ready(function($) {
    $(function() {

        $(".owl-carousel").owlCarousel({
            items: 4,
            loop:true,
            navigation : false,
            slideSpeed : 300,
            paginationSpeed : 400,
            singleItem:true,

            smartSpeed: 1000,


        });

        // $('.owl-next span').html('<img class="arrowsOwlCarousel" src="img/Captureright.png" alt="">')
        // $('.owl-prev span').html('<img  class="arrowsOwlCarousel" src="img/CaptureLeft.png" alt="">')

        $('.dropdown2').click(function () {
            $(this).attr('tabindex', 1).focus();
            $(this).toggleClass('active');
            $(this).find('.dropdown-menu').slideToggle(300);
        });
        $('.dropdown2').focusout(function () {
            $(this).removeClass('active');
            $(this).find('.dropdown-menu').slideUp(300);
        });
        $('.dropdown2 .dropdown-menu li').click(function () {
            $(this).parents('.dropdown').find('span.text').text($(this).text());
            $(this).parents('.dropdown').find('input').attr('value', $(this).attr('id'));
            $(this).parents('.dropdown').find('input').attr('value', $(this).attr('class'));
            // alert($(this).attr('class'))
            var admin_code = $('#admin_code').val();
            var user_report = $('#user_report').val();

            localStorage.setItem("user_report", user_report);
            var user = [];
            $('.dropdownForCheckWorkers .checked').each(function (k) {
                user[k] = $(this).val();
            });

            var arr=[];
            $('.dropdown-menu.drop1 input').each(function (m) {
                if($(this).attr('class') == 'checked'){
                    arr.push($(this).val())
                }
            });
            $('#user_name').attr('value',arr);

            // $('#form_user').submit();

        });
        $('.dropdown-menu.select_type li').click(function () {
            $('#form_user').submit();
        })

        $('.typeOfAdd').on('click', function() {
            $('.uploadFile').addClass('show')
        })

        $('.menuIcon').on('click', function() {
            $('.leftSidebar').toggleClass('partial')
            $('.rightSideContent').toggleClass('full')
        })




        $('.apply button').on('click', function(){
            $('.applyedContent').slideUp()
            $('.apply button').show()
            $(this).closest('.item').find('.applyedContent').slideDown();
            $(this).hide()
        })
        $('.closeOPened').on('click', function(){
            $('.applyedContent').slideUp()
            $(this).closest('.item').find('.apply').find('button').show();

        })

    })
})