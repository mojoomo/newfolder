jQuery.noConflict();

jQuery(document).ready(function ($) {
    $(".submenu > a").click(function (e) {
        e.preventDefault();
        var $li = $(this).parent("li");
        var $ul = $(this).next("ul");

        if ($li.hasClass("open")) {
            $ul.slideUp(350);
            $li.removeClass("open");
        } else {
            $(".nav > li > ul").slideUp(350);
            $(".nav > li").removeClass("open");
            $ul.slideDown(350);
            $li.addClass("open");
        }
    });

    if (typeof pageName !== "undefined" && pageName == "portfolio") {
        $.ajax({
            url: "checkDataPortfolio",
            method: "POST",
            data: {"_token": token, "catType": "portfolio", "postType": "portfolio"},
            cache: false,
            success: function (r) {
                if (r.category.length > 0 && r) {
                    r.category.forEach(function (item, count) {
                        $(".portfolioCategories").append(
                            '<li class="item">\n' +
                            item.name +
                            '                                <span class="delete" data-id="' + item.id + '">\n' +
                            '                                    <svg viewBox="0 0 1792 1792" fill="currentColor" xmlns="http://www.w3.org/2000/svg"><path d="M1490 1322q0 40-28 68l-136 136q-28 28-68 28t-68-28l-294-294-294 294q-28 28-68 28t-68-28l-136-136q-28-28-28-68t28-68l294-294-294-294q-28-28-28-68t28-68l136-136q28-28 68-28t68 28l294 294 294-294q28-28 68-28t68 28l136 136q28 28 28 68t-28 68l-294 294 294 294q28 28 28 68z"/></svg>\n' +
                            '                                </span>\n' +
                            '                            </li>'
                        );
                    });
                }

                if (r.posts.length > 0 && r) {
                    let options = [];
                    for (let i = 0; i < $(".portfolioCategories .item").length; i++) {
                        let text = $(".portfolioCategories .item").eq(i).text();
                        let idCategory = $(".portfolioCategories .item").eq(i).children(".delete").data("id");
                        options.push('<option data-id="' + idCategory + '" value="' + i + '">' + text.trim() + '</option>');
                    }
                    r.posts.forEach(function (item, count) {
                        $(".linksList").append(
                            ' <li class="item" data-id="' + item.id + '">\n' +
                            '                                <div class="linkCatBlock" data-id="' + item.id + '">\n' +
                            '                                    <a href="javascript: void (0);">' + item.link + '</a>\n' +
                            '                                    <select name="" class="selectpicker form-control linksSelect" data-width="auto" data-id="' + item.id + '" multiple title="Choose one of the following..." >\n' +
                            options.join('') +
                            '                                    </select>\n' +
                            '                                </div>\n' +
                            '                                <span class="saveIcon" data-id="' + item.id + '">' +
                            '                                   <img src="' + saveImage + '" alt="">' +
                            '                                </span>\n' +
                            '                                <span class="delete" data-id="' + item.id + '">\n' +
                            '                                    <svg viewBox="0 0 1792 1792" fill="currentColor" xmlns="http://www.w3.org/2000/svg"><path d="M1490 1322q0 40-28 68l-136 136q-28 28-68 28t-68-28l-294-294-294 294q-28 28-68 28t-68-28l-136-136q-28-28-28-68t28-68l294-294-294-294q-28-28-28-68t28-68l136-136q28-28 68-28t68 28l294 294 294-294q28-28 68-28t68 28l136 136q28 28 28 68t-28 68l-294 294 294 294q28 28 28 68z"/></svg>\n' +
                            '                                </span>\n' +
                            '                            </li>'
                        );

                    });

                }

                // function getUniqTags(tags) {
                //     var results = [];
                //
                //     tags.forEach(function (value) {
                //         value = value.trim();
                //
                //         if (results.indexOf(value) === -1) {
                //             results.push(value);
                //         }
                //     });
                //
                //     return results;
                // }
                function uniq(array) {
                    return array.reduce((acc, current) => {
                        current = current.trim();
                        if (!acc.includes(current)) {
                            acc.push(current);
                        }
                        return acc;
                    }, []);
                }

                setTimeout(function () {
                    r.relation.filter(function (el) {
                        $(".linksList .item").each(function (step, item) {
                            if (el.postId == $(item).data("id")) {
                                $(item).find(".linksSelect option").each(function (count, elem) {
                                    if (el.catId == $(elem).data("id")) {
                                        $(elem).filter(`[data-id='${el.catId}']`).prop('selected', true);
                                        $(elem).parent().selectpicker('refresh');
                                    }
                                })
                            }
                        });
                    });
                }, 1000);

                $('.linksSelect').selectpicker('refresh');
            }
        }).error(function (err, status, error) {

        });


        //create Category
        $(".portfolio .catSaveBtn button").on("click touchstart", function () {

            if ($(".websiteCategory input").val() == "")
                return false;

            $.ajax({
                url: "savePortfolioCategory",
                method: "POST",
                data: {
                    "_token": token,
                    "categoryName": $(".websiteCategory input").val(),
                    "categoryType": "portfolio",
                },
                cache: false,
                success: function (result) {
                    $(".portfolioCategories").append(
                        '<li class="item">\n' +
                        result.category.name +
                        '                                <span class="delete" data-id="' + result.category.id + '">\n' +
                        '                                    <svg viewBox="0 0 1792 1792" fill="currentColor" xmlns="http://www.w3.org/2000/svg"><path d="M1490 1322q0 40-28 68l-136 136q-28 28-68 28t-68-28l-294-294-294 294q-28 28-68 28t-68-28l-136-136q-28-28-28-68t28-68l294-294-294-294q-28-28-28-68t28-68l136-136q28-28 68-28t68 28l294 294 294-294q28-28 68-28t68 28l136 136q28 28 28 68t-28 68l-294 294 294 294q28 28 28 68z"/></svg>\n' +
                        '                                </span>\n' +
                        '                            </li>'
                    );

                    $("select.linksSelect").append('<option data-id="' + result.category.id + '">' + $(".websiteCategory input").val().trim() + '</option>');
                    $('select.linksSelect').selectpicker('refresh');
                }
            }).error(function (err, status, error) {

            });
        });


        //Delete Category
        $('body').delegate(".portfolioCategories span.delete", "click touchstart", function () {
            let that = $(this);
            $.ajax({
                url: "delCategory",
                method: "POST",
                data: {
                    "_token": token,
                    "delId": $(this).data("id")
                },
                cache: false,
                success: function (res) {
                    that.closest('.item').remove();
                    $(".linksSelect option[data-id=" + that.data('id') + "]").remove();
                    $(".linksSelect").selectpicker('refresh');
                }
            }).error(function (err, status, error) {

            })
        });


        // Create Link
        $(".portfolio .linkSaveBtn button").on("click touchstart", function () {

            if ($(".websiteLink input").val() == "")
                return false;

            $.ajax({
                url: "createPortfolioLink",
                method: "POST",
                data: {
                    "_token": token,
                    "link": $(".websiteLink input").val(),
                    "pageName": pageName,
                    "postType": "portfolio",
                },
                cache: false,
                success: function (response) {
                    let options = [];
                    for (let i = 0; i < $(".portfolioCategories .item").length; i++) {
                        let text = $(".portfolioCategories .item").eq(i).text();
                        let idCategory = $(".portfolioCategories .item").eq(i).children(".delete").data("id");
                        options.push('<option data-id="' + idCategory + '">' + text.trim() + '</option>');
                    }
                    $(".linksList").append(
                        ' <li class="item">\n' +
                        '                                <div class="linkCatBlock" data-id="' + response.post.id + '">\n' +
                        '                                    <a href="javascript: void (0);">' + response.post.link + '</a>\n' +
                        '                                    <select name="" class="selectpicker form-control linksSelect" data-width="auto" data-id="' + response.post.id + '" multiple title="Shoose item...">\n' +
                        options.join('') +
                        '                                    </select>\n' +
                        '                                </div>\n' +
                        '                                <span class="saveIcon" data-id="' + response.post.id + '">' +
                        '<img src="' + saveImage + '" alt="">' +
                        '                                </span>\n' +
                        '                                <span class="delete" data-id="' + response.post.id + '">\n' +
                        '                                    <svg viewBox="0 0 1792 1792" fill="currentColor" xmlns="http://www.w3.org/2000/svg"><path d="M1490 1322q0 40-28 68l-136 136q-28 28-68 28t-68-28l-294-294-294 294q-28 28-68 28t-68-28l-136-136q-28-28-28-68t28-68l294-294-294-294q-28-28-28-68t28-68l136-136q28-28 68-28t68 28l294 294 294-294q28-28 68-28t68 28l136 136q28 28 28 68t-28 68l-294 294 294 294q28 28 28 68z"/></svg>\n' +
                        '                                </span>\n' +
                        '                            </li>'
                    );
                    $('.linksSelect').selectpicker('refresh');
                }
            }).error(function (err, status, error) {

            });
        });


        //Delete Link
        $('body').delegate(".portfolio .linksList span.delete", "click touchstart", function (evt) {
            evt.preventDefault();
            evt.stopPropagation();

            let that = $(this);
            $.ajax({
                url: "delLink",
                method: "POST",
                data: {
                    "_token": token,
                    "delId": $(this).data("id")
                },
                cache: false,
                success: function (res) {
                    that.closest('.item').remove();
                }
            }).error(function (err, status, error) {

            });
        });

        function removeItemOnce(arr, value) {
            var index = arr.indexOf(value);
            if (index > -1) {
                arr.splice(index, 1);
            }
            return arr;
        }

        // when change links select
        let activeOptions = [];
        let deletable = [];
        $(document).on("change", ".linkCatBlock .linksSelect", function (evt) {
            evt.preventDefault();
            evt.stopPropagation();

            const distinct = (value, index, self) => {
                return self.indexOf(value) === index;
            };
            $(this).children('option').each(function (count, item) {
                // let selected = $(this).eq(count).prop("selected");
                if (item.selected) {
                    activeOptions.push(item.getAttribute("data-id"));
                    item.classList.add("added");
                } else {
                    activeOptions = removeItemOnce(activeOptions, item.getAttribute("data-id"))
                    // activeOptions.pop();
                    item.classList.remove("added");
                }
            });
            activeOptions = activeOptions.filter(distinct);
        });

        let urlPortfolio;
        $(document).on("click touchstart", ".linksList .saveIcon", function (evt) {
            evt.preventDefault();
            evt.stopPropagation();

            // $(this).selectpicker('refresh');
            let a = [];
            $(this).siblings(".linkCatBlock").find(".linksSelect option").each(function (count, item) {
                if ($(item).prop('selected')) {
                    a.push($(item).data("id"))
                }
            })
            const distinct = (value, index, self) => {
                return self.indexOf(value) === index;
            };
            let options = a.length == 0 ? activeOptions.filter(distinct) : a.filter(distinct);
            // console.log(options);
            // return false;
            $.ajax({
                url: "setRelationsPortfolio",
                method: "POST",
                data: {
                    "_token": token,
                    "postId": $(this).data("id"),
                    "catId": JSON.stringify(options),
                    "deletable": deletable
                },
                cache: false,
                success: function (res) {
                    activeOptions.length = 0;
                    options.length = 0;


                    $(".content-box.notice").addClass("show");
                    $(".content-box.notice.show").css({"border-color":"green", "color":"green"});
                    $(".content-box.notice").text(successSaveMessage);
                    setTimeout(function() {
                        $(".content-box.notice").removeClass("show");
                    }, 3000);
                }
            }).error(function (err, status, error) {

            });
        });
    } //End page Portfolio

    if (typeof pageNameView !== "undefined" && pageNameView == "portfolio") {
        // Page at front side
        Array.prototype.remove = function () {
            var what, a = arguments, L = a.length, ax;
            while (L && this.length) {
                what = a[--L];
                while ((ax = this.indexOf(what)) !== -1) {
                    this.splice(ax, 1);
                }
            }
            return this;
        };


        $(window).on('hashchange', function () {
            if (window.location.hash) {
                var page = window.location.hash.replace('#', '');
                if (page == Number.NaN || page <= 0) {
                    return false;
                } else {
                    getData(page);
                }
            }
        });

        $(document).on('click', '.pagination a', function (event) {
            $(this).each(function (count, item) {
                if (!$(item).hasClass('active')) {
                    event.preventDefault();
                }
            })
            $('li').removeClass('active');
            $(this).parent('li').addClass('active');

            var myurl = $(this).attr('href');
            var page = $(this).attr('href').split('page=')[1];

            getData(page);

        });

        let ids = [];
        let itr = 0;

        function getData(page) {
            $.ajax(
                {
                    url: '?page=' + page,
                    type: "get",
                    datatype: "html",
                })
                .done(function (data) {
                    $(".elements").empty().html(data);
                    location.hash = page;
                })
                .fail(function (jqXHR, ajaxOptions, thrownError) {
                    console.log("fail");
                });
        }

        $("body").delegate(".portfolio .filters button:not(.resetActive)", "click touchstart", function (e) {
            e.preventDefault();
            e.stopPropagation();

            $(this).toggleClass('active');
            if ($(this).hasClass('active')) {
                ids.push($(this).attr("data-id"));
            } else {
                ids.remove($(this).attr("data-id"));
            }

            $.ajax({
                url: pageNameView,
                method: "POST",
                data: {"_token": token, "filterIds": JSON.stringify(ids)},
                cache: false,
                success: function (res) {
                    console.log(res);
                    $(".elements").empty().html(res);
                }
            }).fail(function (err, status, error) {
                console.log(err);
            });

        });

        // $("body").delegate(".portfolio .filters button:not(.resetActive)", "click touchstart", function(e) {
        //     e.preventDefault();
        //     e.stopPropagation();
        //
        //     $(this).toggleClass('active');
        //     if($(this).hasClass('active')) {
        //         ids.push($(this).attr("data-id"));
        //     }else {
        //         ids.remove($(this).attr("data-id"));
        //     }
        //     $.ajax({
        //         url: "portfolioFilter",
        //         method: "POST",
        //         data: { "_token": token, "filterIds": JSON.stringify(ids) },
        //         cache: false,
        //         success: function(res) {
        //             $(".portfolio .portfolioItems").empty().html();
        //             res.posts.forEach(function(item, count) {
        //                 item.posts.forEach(function(it) {
        //                     $(".portfolio .portfolioItems").append('<div class="item" data-id="'+it.id+'">\n' +
        //                         '                    <div class="logoAndLink">\n' +
        //                         '                        <iframe style="width:100%; height: 100%;" src="'+it.link+'"></iframe>\n' +
        //                         '                        <a href="'+it.link+'">\n' +
        //                         '                            <span>View Website</span>\n' +
        //                         '                            <span>\n' +
        //                         '<i class="fas fa-chevron-right"></i>\n' +
        //                         '</span>\n' +
        //                         '                        </a>\n' +
        //                         '                    </div>\n' +
        //                         '                    <div class="itemImageBackground" style="background-image: url(./../assets/images/portfolio/ITResources.png);">\n' +
        //                         '\n' +
        //                         '                    </div>\n' +
        //                         '                </div>');
        //
        //                 });
        //             })
        //         }
        //     }).fail(function (err, status, error) {
        //         console.log(err);
        //     });
        //     // console.log(ids);
        // });

        $("body").delegate(".resetActive", "click touchstart", function () {
            // $(this).siblings().removeClass('active');
            location.reload();
        });

    }// End page Portfolio View

    // page Careers
    if (typeof pageName !== "undefined" && pageName == "careers") {
        let title = titleTrans,
            date = dateTrans,
            loc = locationTrans,
            qualification = qualificationsTrans,
            contentTranslate = contentTrans,
            saveTranslate = saveTrans;




        $.ajax({
            url: "showCareer",
            type: "POST",
            data: {"_token": token, "type": "careers"},
            cache: false,
            success: function(data) {
                let itmId;

                data.all.forEach(function(item, count) {
                    let content = (language == "hy" ? item.content : (language == "en" ? item.contentEn : (language == "ru" ? item.contentRu : "")));

                    $(".careerPage").append('<div class="content-box-large" data-id="'+item.id+'">\n' +
                        '            <div class="panel-heading">\n' +
                        '                <div class="panel-title"></div>\n' +
                        '<span class="del"><svg viewBox="0 0 1792 1792" fill="currentColor" xmlns="http://www.w3.org/2000/svg">' +
                        '<path d="M1490 1322q0 40-28 68l-136 136q-28 28-68 28t-68-28l-294-294-294 294q-28 28-68 28t-68-28l-136-136q-28-28-28-68t28-68l294-294-294-294q-28-28-28-68t28-68l136-136q28-28 68-28t68 28l294 294 294-294q28-28 68-28t68 28l136 136q28 28 28 68t-28 68l-294 294 294 294q28 28 28 68z"></path>' +
                        '</svg></span>' +
                        '            </div>\n' +
                        '            <div class="panel-body">\n' +
                        '                <section class="careerHeading">\n' +
                        '                    <div class="form-group">\n' +
                        '                        <span>'+title+'</span>\n' +
                        '                        <input type="text" name="titleCareer" value="'+item.name+'" class="form-control">\n' +
                        '                    </div>\n' +
                        '                    <div class="form-group">\n' +
                        '<br/>'+
                        '                        <span>'+date+'</span>\n' +
                        '                        <div class="input-group date" id="datetime'+(count == 0 ? 1 : count)+'picker">\n' +
                        '                            <input type="text" name="dateCareer" value="'+moment(item.date).format('DD/MM/YYYY')+'" class="form-control" />\n' +
                        '                            <span class="input-group-addon">\n' +
                        '                                <span class="glyphicon glyphicon-calendar"></span>\n' +
                        '                            </span>\n' +
                        '                        </div>\n' +
                        '                    </div>\n' +
                        '                    <div class="form-group">\n' +
                        '<br/>'+
                        '                        <span>'+loc+'</span>\n' +
                        '                        <input type="text" name="locationCareer" value="'+item.location+'" class="form-control">\n' +
                        '                    </div>\n' +
                        '                    <div class="form-group qualifications">\n' +
                        '<br/>'+
                        '                        <span>'+qualification+'</span>\n' +
                        '                       <div class="blockQualification"><input type="text" name="skillsCareer" class="form-control qualInput"> <i class="glyphicon glyphicon-floppy-saved"></i></div>\n' +
                        '                    </div>\n' +
                        '                    <div class="form-group passGroup">\n' +
                        '                        <span class="content-title">'+contentTranslate+'</span>\n' +

                        '                           <form> <textarea name="contentCareer" class="careerEditor'+ (count == 0 ? 1 : count) + "_" + (language == "hy" ? "hy" : (language == "en" ? "en" : ( language == "ru" ? "ru" : ""))) +' ">'+content+'</textarea></form>\n' +
                        '                    </div>\n' +
                        '                    <div class="form-group passGroup submitBtn">\n' +
                        '                        <button class="btn btn-primary">'+saveTranslate+'</button>\n' +
                        '                    </div>\n' +
                        '                </section>\n' +
                        '            </div>\n' +
                        '        </div>');

                    if(item.category.length > 0) {
                        setTimeout(function() {
                            itmId = item.id;
                            item.category.forEach(function(elem, count) {
                                $(".careerPage .content-box-large[data-id='"+itmId+"']").find('.qualifications').append('' +
                                    '<span class="careerCat" data-id="'+elem.id+'">'+elem.name+'<span class="delCareerCat">' +
                                    '<svg viewBox="0 0 1792 1792" fill="currentColor" xmlns="http://www.w3.org/2000/svg"><path d="M1490 1322q0 40-28 68l-136 136q-28 28-68 28t-68-28l-294-294-294 294q-28 28-68 28t-68-28l-136-136q-28-28-28-68t28-68l294-294-294-294q-28-28-28-68t28-68l136-136q28-28 68-28t68 28l294 294 294-294q28-28 68-28t68 28l136 136q28 28 28 68t-28 68l-294 294 294 294q28 28 28 68z"></path>' +
                                    '</svg></span></span>');

                            });
                        },200)
                    }

                });

                let editorCareer;
                for(let itr = 1; itr <= $(".careerPage .content-box-large").length; itr++) {
                    $(`#datetime${itr}picker`).datetimepicker({
                        minView: 2,
                        // viewMode: 'days',
                        format: "dd/mm/yyyy",
                        locale: "hy",
                        autoclose: true
                    });
                    editorCareer = $('textarea.careerEditor'+itr+'_'+language).ckeditor({
                        language: language,
                        removePlugins: 'save,newpage,image,help,about'
                    }).editor;
                }
            }
        }).fail(function(err, status, error) {

        });


        $(".careerPage .multiplyIcon ").on("click touchstart", function() {
            let len = $(".careerPage .content-box-large").length;

            $(".careerPage").append('<div class="content-box-large">\n' +
                '            <div class="panel-heading">\n' +
                '                <div class="panel-title"></div>\n' +
                '<span class="del"><svg viewBox="0 0 1792 1792" fill="currentColor" xmlns="http://www.w3.org/2000/svg">' +
                '<path d="M1490 1322q0 40-28 68l-136 136q-28 28-68 28t-68-28l-294-294-294 294q-28 28-68 28t-68-28l-136-136q-28-28-28-68t28-68l294-294-294-294q-28-28-28-68t28-68l136-136q28-28 68-28t68 28l294 294 294-294q28-28 68-28t68 28l136 136q28 28 28 68t-28 68l-294 294 294 294q28 28 28 68z"></path>' +
                '</svg></span>' +
                '            </div>\n' +
                '            <div class="panel-body">\n' +
                '                <section class="careerHeading">\n' +
                '                    <div class="form-group">\n' +
                '                        <span>Title</span>\n' +
                '                        <input type="text" name="titleCareer" class="form-control">\n' +
                '                    </div>\n' +
                '                    <div class="form-group">\n' +
                '<br/>'+
                '                        <span>Date</span>\n' +
                '                        <div class="input-group date" id="datetime'+len+'picker">\n' +
                '                            <input type="text" name="dateCareer" class="form-control" />\n' +
                '                            <span class="input-group-addon">\n' +
                '                                <span class="glyphicon glyphicon-calendar"></span>\n' +
                '                            </span>\n' +
                '                        </div>\n' +
                '                    </div>\n' +
                '                    <div class="form-group">\n' +
                '<br/>'+
                '                        <span>Location</span>\n' +
                '                        <input type="text" name="locationCareer" class="form-control">\n' +
                '                    </div>\n' +
                '                    <div class="form-group qualifications">\n' +
                '<br/>'+
                '                        <span>Qualifications</span>\n' +
                '                       <div class="blockQualification"><input type="text" name="skillsCareer" class="form-control qualInput"> <i class="glyphicon glyphicon-floppy-saved"></i></div>\n' +
                '                    </div>\n' +
                '                    <div class="form-group passGroup">\n' +
                '                        <span class="content-title">Content</span>\n' +

                '                           <form> <textarea name="contentCareer" class="careerEditor'+ len + "_" + (language == "hy" ? "hy" : (language == "en" ? "en" : ( language == "ru" ? "ru" : ""))) +' "></textarea></form>\n' +
                '                    </div>\n' +
                '                    <div class="form-group passGroup submitBtn">\n' +
                '                        <button class="btn btn-primary">Save</button>\n' +
                '                    </div>\n' +
                '                </section>\n' +
                '            </div>\n' +
                '        </div>');

            let editorCareer;
            for(let itr = 1; itr <= $(".careerPage .content-box-large").length; itr++) {
                $(`#datetime${itr}picker`).datetimepicker({
                    minView: 2,
                    // viewMode: 'days',
                    format: "dd/mm/yyyy",
                    locale: "hy",
                    autoclose: true
                });
                editorCareer = $('textarea.careerEditor'+itr+'_'+language).ckeditor({
                    language: language,
                    removePlugins: 'save,newpage,image,help,about'
                }).editor;
            }
        });

        // Delete career
        $("body").delegate(".careerPage span.del", "click touchstart", function(){
            let attr = $(this).closest(".content-box-large").attr('data-id');
            let that = $(this);
            if(typeof attr !== typeof undefined && attr !== false) {
                $.ajax({
                    url: "destroyCareer",
                    type: "POST",
                    data: {"_token": token, "delId": $(this).closest(".content-box-large").data('id')},
                    cache: false,
                    success: function(resp) {
                        that.closest(".content-box-large").remove();
                    }
                }).fail(function(err, status, error) {
                    console.log(err);
                });
            }else {
                $(this).closest(".content-box-large").remove();
            }
        });

        let catArr = [];
        let newCat = [];
        // make category
        $("body").delegate(".careerPage .qualifications i", "click touchstart", function() {
            let dataId = $(this).closest('.content-box-large').attr("data-id");
            let inputValue = $(this).siblings('.qualInput').val();
            if(typeof dataId !== typeof undefined && dataId !== false) {
                newCat.push(inputValue);
            }

            if(inputValue == "")
                return false;
            $(this).parent().parent().append('<span class="careerCat" data-id="">'+$(this).siblings('.qualInput').val()+' <span class="delCareerCat"><svg viewBox="0 0 1792 1792" fill="currentColor" xmlns="http://www.w3.org/2000/svg">' +
                '<path d="M1490 1322q0 40-28 68l-136 136q-28 28-68 28t-68-28l-294-294-294 294q-28 28-68 28t-68-28l-136-136q-28-28-28-68t28-68l294-294-294-294q-28-28-28-68t28-68l136-136q28-28 68-28t68 28l294 294 294-294q28-28 68-28t68 28l136 136q28 28 28 68t-28 68l-294 294 294 294q28 28 28 68z"></path>' +
                '</svg></span></span>  ');
            // catArr.push(inputValue);
            // $.ajax({
            //     url: "careerCatSave",
            //     type: "POST",
            //     data: {"_token": token,
            //         "name": inputValu,
            //         "type": "career"
            //     },
            //     cache: false,
            //     success: function(data) {
            //         console.log(data);
            //         that.parent().parent().append('<span class="careerCat" data-id="'+data.currentId+'">'+$(this).siblings('.qualInput').val()+' <span class="delCareerCat"><svg viewBox="0 0 1792 1792" fill="currentColor" xmlns="http://www.w3.org/2000/svg">' +
            //             '<path d="M1490 1322q0 40-28 68l-136 136q-28 28-68 28t-68-28l-294-294-294 294q-28 28-68 28t-68-28l-136-136q-28-28-28-68t28-68l294-294-294-294q-28-28-28-68t28-68l136-136q28-28 68-28t68 28l294 294 294-294q28-28 68-28t68 28l136 136q28 28 28 68t-28 68l-294 294 294 294q28 28 28 68z"></path>' +
            //             '</svg></span></span>  ');
            //     }
            // }).fail(function(err, status, error) {
            //     console.log(err);
            // })
        });


        // Delete Career Category
        $("body").delegate(".careerPage .careerCat .delCareerCat", "click touchstart", function() {
            let that = $(this);
            if(typeof $(this).parent().data('id') !== typeof undefined && $(this).parent().data('id') > 0) {
                $.ajax({
                    url: "delCareerCat",
                    type: "POST",
                    data: {"_token": token, "delCat": $(this).parent().attr('data-id')},
                    cache: false,
                    success: function(response) {
                        console.log(response);
                        that.parent().remove();
                    }
                }).fail(function(err, status, error) {
                    console.log(err);
                });
            }else {
                newCat.pop();
                that.parent().remove();
            }
            console.log(newCat);
        });

        $("body").delegate(".careerPage .content-box-large .submitBtn", "click touchstart", function(evt) {
            evt.preventDefault();
            evt.stopPropagation();

            let careerUrl;
            let that = $(this);

            let elem = $(this).siblings(".qualifications").find('.careerCat');
            for(let j = 0; j < elem.length; j++ ) {
                catArr.push(elem[j].innerText);
            }
            let skillIds = [];
            let hasDataId = $(this).closest('.content-box-large').attr('data-id');
            if(typeof hasDataId !== typeof undefined && hasDataId !== false) {
                careerUrl = "updateCareer";
                for(let j = 0; j < elem.length; j++) {
                    skillIds.push(elem.attr('data-id'));
                }
            }else {
                careerUrl = "storeCareer";
            }

            let careerId = $(this).closest(".content-box-large").data("id");
            let titleField = $(this).siblings(".form-group").find('input[name="titleCareer"]').val();
            let dateFieald = $(this).siblings(".form-group").find('input[name="dateCareer"]').val();
            let locationField = $(this).siblings(".form-group").find('input[name="locationCareer"]').val();
            let content = $(this).siblings(".form-group").find('textarea[class*="_'+language+'"]').val();

            // console.log(content);
            $.ajax({
                url: careerUrl,
                type: "POST",
                data: {"_token": token,
                "title": titleField,
                "date": dateFieald,
                "location": locationField,
                "skills": JSON.stringify(catArr),
                "catType": "career",
                "textContent": `${content}`,
                "newCat": JSON.stringify(newCat),
                "skillIds": JSON.stringify(skillIds),
                "careerId": careerId,
                "lang": language,
                "pageName": pageName,
                },
                cache: false,
                success: function(res) {
                    console.log(res.fullData);
                    res.fullData.forEach(function(item, count) {
                        that.closest('.careerHeading').attr('data-id', item.id);
                        let content = (language == "hy" ? item.content : (language == "en" ? item.contentEn : (language == "ru" ? item.contentRu : "")));
                        that.closest('input[name="titleCareer"]').val(item.name);
                        that.closest('input[name="dataCareer"]').datetimepicker('refresh');
                        that.closest('input[name="dataCareer"]').val(item.date);
                        that.closest('input[name="locationCareer"]').val(item.location);
                        that.closest('input[name="contentCareer"]').val(content);
                        item.category.forEach(function(elem, step) {
                            that.closest('.qualifications').append('' +
                                '<span class="careerCat" data-id="'+elem.id+'">'+elem.name+'<span class="delCareerCat">' +
                                '<svg viewBox="0 0 1792 1792" fill="currentColor" xmlns="http://www.w3.org/2000/svg">' +
                                '<path d="M1490 1322q0 40-28 68l-136 136q-28 28-68 28t-68-28l-294-294-294 294q-28 28-68 28t-68-28l-136-136q-28-28-28-68t28-68l294-294-294-294q-28-28-28-68t28-68l136-136q28-28 68-28t68 28l294 294 294-294q28-28 68-28t68 28l136 136q28 28 28 68t-28 68l-294 294 294 294q28 28 28 68z"></path></svg>' +
                                '</span></span>');
                        });
                        that.closest("textarea[name='contentCareer']").val(language == "hy" ? item.content : (language == "en" ? item.contentEn : ( language == "ru" ? item.contentRu : "")));


                    });
                    if(typeof hasDataId !== typeof undefined && hasDataId !== false) {
                        careerUrl = "updateCareer";
                        for(let j = 0; j < elem.length; j++) {
                            skillIds.push(elem.attr('data-id'));
                        }
                    }else {
                        careerUrl = "storeCareer";
                    }
                    // location.reload();
                }
            }).fail(function(err, status, error){
                console.log(error);
            });

        });

    } // end page Careers

    if(typeof pageNameView !== "undefined" && pageNameView == "services") {

        let serviceName = $(".applyForService input[name='serviceFullName']");
        let serviceEmail = $(".applyForService input[name='serviceEmail']");
        let serviceWebsite = $(".applyForService input[name='serviceWebsite']");
        let servicePhone = $(".applyForService input[name='servicePhone']");
        let serviceCategoryVal = $(".applyForService .dropdown2 .select .text").text();
        let serviceText = $(".applyForService textarea[name='serviceText']");
        let categBool = false;
        $("body").delegate(".applyForService .dropdown-menu .serviceItem", "click touchstart", function() {
            categBool = true;
        });
        $(".applyForService #serviceForm .item button[type='button']").on("click touchstart", function(e) {
            e.preventDefault();
            e.stopPropagation();

            $.ajax({
                url: "services",
                type: "POST",
                data: {"_token": token,
                "serviceFullName": serviceName.val(),
                "serviceEmail": serviceEmail.val(),
                "serviceWebsite": serviceWebsite.val(),
                "servicePhone": servicePhone.val(),
                "serviceCategoryVal": categBool ? serviceCategoryVal: "",
                "serviceText": serviceText.val()
                },
                cache: false,
                success: function(data) {
                    if( Object.keys(data.success).length > 0 ) {
                        $("#serviceForm .serviceSuccess").text(data.message);
                        
                        setTimeout(function() {
                            $("#serviceForm .serviceSuccess").css({"display": "none", "color":"green"});
                            location.reload();
                        }, 3000);
                    }
                }
            }).fail(function(err, status, error) {

                $("#serviceForm .errorNoticeServices").text(err.responseJSON.erorrServices).css({"color": "red"});

                if(typeof err.responseJSON.allErr.serviceFullName != typeof undefined && err.responseJSON.allErr.serviceFullName.length > 0) {
                    serviceName.css({"border-color":"red"});
                }
                if(typeof err.responseJSON.allErr.serviceEmail != typeof undefined && err.responseJSON.allErr.serviceEmail.length > 0) {
                    serviceEmail.css({"border-color": "red"});
                }
                // if(typeof err.responseJSON.allErr.serviceWebsite != typeof undefined && err.responseJSON.allErr.serviceWebsite.length > 0) {
                //     serviceWebsite.css({"border-color":"red"});
                // }
                if(typeof err.responseJSON.allErr.servicePhone != typeof undefined && err.responseJSON.allErr.servicePhone.length > 0) {
                    servicePhone.css({"border-color":"red"});
                }
                if(typeof err.responseJSON.allErr.serviceText != typeof undefined && err.responseJSON.allErr.serviceText.length > 0) {
                    serviceText.css({"border-color":"red"});
                }
                if(typeof err.responseJSON.allErr.serviceCategoryVal != typeof undefined && err.responseJSON.allErr.serviceCategoryVal.length > 0) {
                    $(".applyForService .dropdown2").css({"border-color":"red"});
                }

            });
        });
    }


    if(typeof pageName !== "undefined" && pageName == "services") {
        $(".servicesPageAdmin .applicantFullInfo .removeApplicant").on("click touchstart", function() {
            let that = $(this);
            $.ajax({
                url: "removeApplicant",
                type: "POST",
                data: {"_token": token, "deleteId": $(this).data('id')},
                cache: false,
                success: function(data) {
                    that.parent().remove();
                }
            }).fail(function(err, status, error) {

            });
        });
    }

    // Home View page
    if (typeof pageNameView !== "undefined" && pageNameView == "") {
        $("#homeMailForm input[type='submit']").on("click touchstart", function(e) {
            e.preventDefault();
            e.stopPropagation();
            let uri = contactMail;//`${fullUrl}/sendMail`;
            $.ajax({
                url: contactMail,
                type: "POST",
                data: {"_token":token,
                    "contacterName": $("#homeMailForm input[name='contacterName']").val(),
                    "email": $("#homeMailForm input[name='email']").val(),
                    "message": $("#homeMailForm textarea[name='message']").val()
                },
                cache: false,
                success: function(d) {
                    $("#homeMailForm input[type='text']").css({"border-color":""});
                    $("form#homeMailForm")[0].reset();    
                    $(".formToContact .content-box.notice").addClass("show");
                    $(".formToContact .content-box.notice.show").css({"border-color":"green", "color":"green"});
                    $(".formToContact .content-box.notice").text(d.success);
                    setTimeout(function() {
                        $(".formToContact .content-box.notice").removeClass("show");
                    }, 3000);
                    $("#homeMailForm .nameValid").text("");
                    $("#homeMailForm .emailValid").text("");
                    $("#homeMailForm .messageValid").text("");
                    $("#homeMailForm input[name='contacterName']").css({"border-color": ""});
                    $("#homeMailForm input[name='email']").css({"border-color": ""});
                    $("#homeMailForm textarea[name='message']").css({"border-color": ""});
                }
            }).fail(function(err, status,error) {
                if(typeof err.responseJSON.contactError != typeof undefined && err.responseJSON.contactError != "" ) {
                    $(".formToContact .content-box.notice").addClass("show");
                    $(".formToContact .content-box.notice.show").css({"border-color":"red", "color":"red"});
                    $(".formToContact .content-box.notice").text(err.responseJSON.contactError);
                    setTimeout(function() {
                        $(".formToContact .content-box.notice").removeClass("show");
                    }, 3000);
                }
                if(typeof err.responseJSON.allErr.contacterName != typeof undefined && err.responseJSON.allErr.contacterName.length != "") {
                    $("#homeMailForm input[name='contacterName']").css({"border-color": "red"});
                    $("#homeMailForm .nameValid").css({"color":"red"});    
                    $("#homeMailForm .nameValid").text(err.responseJSON.allErr.contacterName[0]);
                }else {
                    $("#homeMailForm .nameValid").text('');
                    $("#homeMailForm input[name='contacterName']").css({"border-color": ""});
                }

                if(typeof err.responseJSON.allErr.email != typeof undefined && err.responseJSON.allErr.email.length != "") {
                    $("#homeMailForm input[name='email']").css({"border-color": "red"});
                    $("#homeMailForm .emailValid").css({"color":"red"});    
                    $("#homeMailForm .emailValid").text(err.responseJSON.allErr.email[0]);
                }else {
                    $("#homeMailForm .emailValid").text('');
                    $("#homeMailForm input[name='email']").css({"border-color": ""});
                }
                if(typeof err.responseJSON.allErr.message != typeof undefined && err.responseJSON.allErr.message.length != "") {
                    $("#homeMailForm textarea[name='message']").css({"border-color": "red"});
                    $("#homeMailForm .messageValid").css({"color":"red"});    
                    $("#homeMailForm .messageValid").text(err.responseJSON.allErr.message[0]);
                }else {
                    $("#homeMailForm .messageValid").text('');
                    $("#homeMailForm textarea[name='message']").css({"border-color": ""});
                }

            })                
        });            
    }

    //inputSearch
    if(typeof pageNameView !== "undefined" && pageNameView == "careers") {

        $("div.search button").on("click touchstart", function(e) {
            e.preventDefault();
            e.stopPropagation();
            let searchVal = $(".search .inputSearch").val();
            if(searchVal == "")
                return false;
            $.ajax({
                url: "searchCareer",
                type: "GET",
                data: {"_token" : token, "q": searchVal},
                cache: false,
                success: function(data) {
                    $(".careersItem .item").each(function(step, elem) {
                        data.srch.forEach(function(item, count) {
                            if(String($(elem).find(".title p").text()).toLowerCase() == String(item.name.toLowerCase())) {
                                $(".careersItem").html('<div class="item">'+$(elem).html()+'</div>');
                            }
                        });
                    });

                }
            }).fail(function(err, status, error) {
                console.log(err);
            });
        });

        $(".search .inputSearch").on("focusout", function() {
            if($(this).val() == "")
                location.reload();
        });

    }

    // if(typeof pageNameView !== "undefined" && pageNameView == "portfolio") {
    //     $("div.search button").on("click touchstart", function(e) {
    //         e.preventDefault();
    //         e.stopPropagation();
    //         let searchVal = $(".search .inputSearch").val();
    //
    //         $.ajax({
    //             url: "searchPortfolio",
    //             type: "GET",
    //             data: {"_token" : token, "q": searchVal},
    //             cache: false,
    //             success: function(data) {
    //                 console.log(data);
    //
    //             }
    //         }).fail(function(err, status, error) {
    //             console.log(err);
    //         });
    //     });
    // }

});

